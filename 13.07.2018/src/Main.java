public class Main {

    public static void main(String[] args) {
        String przykladowyKodPocztowy = "90-123";

        kodPocztowy("12-123");
        kodPocztowy("12123");
        kodPocztowy("121-23");

        numerTelefonu("123-123-132");
        numerTelefonu("123123132");

        adresIp("133.133.123.132");
        adresIp("13.13.13.12");
        adresIp("3+3}3/2");

        nazywamSie("barbara barbara barbara");
        nazywamSie("cezary ezary Co");

        zamieszkanie("UL. zgierska 12");
        zamieszkanie("zgierska 12");
        zamieszkanie("D--sgierska 12");

    }

    static void kodPocztowy(String napis) {
        System.out.print(napis + " >> ");
        System.out.println(napis.matches("[0-9]{2}-?[0-9]{3}"));
    }

    static void numerTelefonu(String napis) {
        System.out.print(napis + " >> ");
        System.out.println(napis.matches("([0-9]{3}-){2}[0-9]{3}|[0-9]{9}"));   //numer telefonu mozna podac z lub bez myslnikow
    }

    static void adresIp(String napis) {
        System.out.print(napis + " >> ");
        System.out.println(napis.matches("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}")); //testowanie adresu ip
    }

    static void nazywamSie(String napis) {
        System.out.print(napis + " >> ");
        System.out.println(napis.matches("[A-Z][a-z]+ ([A-Z][a-z]+ )?[A-Z][a-z]+")); // testowanie jak kolo sie loguje imieniem i nazwiskiem
    }

    static void zamieszkanie(String napis) {
        System.out.print(napis + " >> ");
        System.out.println(napis.matches("((AL|PL|OS|UL)\\. )?.+ [0-9]+")); // adres zamieszkania
    }

    static void adresEmail(String napis) {
        System.out.print(napis + " >> ");
        System.out.println(napis.matches("([a-z0-9]+[._]?[a-z0-9]+)+@([a-z0-9]+[._]?[a-z0-9]+)+\\.[a-z]{2,}"));  //sprawdzanie poprawnosci adresu email
    }

    static void www(String napis) {
        System.out.print(napis + " >> ");
        System.out.println(napis.matches("(https?://)?(www\\.)?([a-z0-9]+\\.)+[a-z]{2,}"));
    }

    public static int countSmileys(List<String> arr) {
        arr.
        return 0;
    }


}
