package grupaSDA;

public class Main {

    public static void main(String[] args) {
        // main method to show method bellow

        exercise8("Tomaszek", " Sabaka");
        exercise9(3, 2);
        exercise7(6, 3.3f);

    }

    static void exercise7(int x, float y) {
        System.out.println(x + "+" + y + "=" + (x + y));
        System.out.println(x + "*" + y + "=" + (x * y)+"\n");
    }

    static void exercise8(String firstName, String secondName) {
        String concatenation = firstName + secondName;
        System.out.println("Nazywam sie: " + concatenation+"\n");
    }

    static void exercise9(int a, int b) {
        System.out.println(a + "-" + b + "=" + (a - b));
        System.out.println(a + "+" + b + "=" + (a + b));
        System.out.println(a + "*" + b + "=" + (a * b)+"\n");
    }
}
