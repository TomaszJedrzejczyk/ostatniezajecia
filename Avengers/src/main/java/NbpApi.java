import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface NbpApi {
    @GET("tables/a/?format=json")
    Call<List<ListOfRates>> pobierzWaluty();        //nasz call musi zaczynac sie od listy poniewaz nasz json rowniez zaczyna sie od listy
}
