import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter

public class Rate {
    @SerializedName("currency")
    private String currensy;

    @SerializedName("code")
    private String code;

    @SerializedName("mid")
    private double averageRate;
}
