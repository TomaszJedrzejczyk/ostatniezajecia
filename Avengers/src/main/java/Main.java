import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        ListOfRates listOfRates = pobierzDane();
    }

    private static ListOfRates pobierzDane() {
        Retrofit retrofit = new Retrofit.Builder()     //pobiera dane ze striny
                .baseUrl("http://api.nbp.pl/api/exchangerates/")        //dajem tylko do takiej samej wspolnej czesci
                .addConverterFactory(GsonConverterFactory.create())     //konwrtuje jsony
                .build();

        NbpApi api = retrofit.create(NbpApi.class);     //wywolujemy retrofit aby skleic poczatek z reszta naszego adresu

        Call<List<ListOfRates>> ratesCall = api.pobierzWaluty();
        try {
            Response<List<ListOfRates>> response = ratesCall.execute();
            showResults(response.body().get(0));
        } catch (Exception e) {
            e.printStackTrace();
        }


//        ratesCall.enqueue(new Callback<List<ListOfRates>>() {
//            public void onResponse(Call<List<ListOfRates>> call, Response<List<ListOfRates>> response) {
//                if (response.isSuccessful()) {
//                    if (response.body() != null) {
//                        showResults(response.body().get(0));
//                    } else {
//                        System.out.println("Response body is null");
//                    }
//                } else
//                    System.err.println(response.errorBody());   //dzieki err wiadomosc wyswietli sie na czerwono
//            }
//
//            public void onFailure(Call<List<ListOfRates>> call, Throwable throwable) {
//
//            }
//        });
        return null;
    }

    private static void showResults(ListOfRates response) {
        System.out.println("Table: " + response.getTable());
        System.out.println("Date: "+response.getDate());
        System.out.println("No: "+response.getNo());

        for (Rate rate : response.getRateList()) {
            System.out.printf("%.5f\t - \t %-35s - %s\n",
                    rate.getAverageRate(),
                    rate.getCurrensy(),
                    rate.getCode());
        }

    }
}
