package grupaSDA;

import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        boolean[][] wygenerowanaPlansza = generatorPlanszy(4, 4);
        wyswietlPlanszeSaper(wygenerowanaPlansza);

    }

    static boolean[][] generatorPlanszy(int wysokosc, int szerokosc) {
        boolean[][] plansza = new boolean[wysokosc][szerokosc];
        int zageszczenie = 4;
        int liczbaBomb = wysokosc * szerokosc / zageszczenie;
        Random random = new Random();

        for (int i = 0; i < liczbaBomb; i++) {
            int x = random.nextInt(wysokosc);
            int y = random.nextInt(szerokosc);
            if (plansza[x][y]) {
                i--;
            } else {
                plansza[x][y] = true;
            }
        }
        return plansza;
    }

    static void wyswietlPlanszeSaper(boolean[][] plansza) {
        for (int i = 0; i < plansza.length; i++) {
            for (int j = 0; j < plansza[0].length; j++) {
                System.out.print(plansza[i][j] ? " x" : " -");
            }
            System.out.println();
        }
    }

    static int[][] wyswietlLiczby(boolean[][]plansza) {
        int [][] zwracanaPlansza = new int[plansza.length][plansza[0].length];

        for (int i = 0; i < plansza.length; i++) {
            for (int j = 0; j < plansza[0].length; j++) {
                if (plansza[i][j]){
                    zwracanaPlansza[i][j]=-1;
                }else {
                    zwracanaPlansza[i][j]= policzPole(plansza,i,j);
                }
            }
        }
        return zwracanaPlansza;
    }

    private static int policzPole(boolean[][] plansza, int i, int j) { //metoda ktora liczy pole dookola -1
        return 0;
    }

}
