package grupaSDA;

import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        int[] tablicaDo65a = new int[]{1, 2, 3, 4,5,6,7,8};
        System.out.println();
//        int[] tablicaDo65b = new int[]{5, 6, 7, 8,};
//        System.out.println(Arrays.toString(zadanie65(tablicaDo65a, tablicaDo65b)));

//        int[] tablicaDo66a = new int[]{9, 9, 0, 4,};
//        int[] tablicaDo66b = new int[]{6, 7, 8,};
//        System.out.println(Arrays.toString(zadanie66(tablicaDo66a, tablicaDo66b)));

//        boolean[][] tablica = zadanie69(3, 3);
//        wyswietlacz(tablica);

//        System.out.println(Arrays.toString(zadanie68(tablica)));

        //  System.out.println(Arrays.toString(zdanieDodatkowe1(10)));


    }

    static int[] zadanie65(int[] tablica1, int[] tablica2) {
        int[] zwracanaTablica = new int[tablica1.length];
        for (int i = 0; i < tablica1.length; i++) {
            zwracanaTablica[i] = tablica1[i] + tablica2[i];
        }
        return zwracanaTablica;
    }

    static int[] zadanie66(int[] tablica1, int[] tablica2) {
        int[] zwracanaTablica;
        int roznicaDlugosci = Math.abs(tablica1.length - tablica2.length);
        int[] tablicaDluga;
        int[] tablicaKrotka;


        if (tablica1.length > tablica2.length) {
            tablicaDluga = tablica1;
            tablicaKrotka = tablica2;
        } else {
            tablicaKrotka = tablica2;
            tablicaDluga = tablica1;
        }
        zwracanaTablica = new int[tablicaDluga.length];

        int reszta = 0;
        for (int i = zwracanaTablica.length - 1; i >= 0; i--) {
            if (i - roznicaDlugosci >= 0) {
                int wynik = tablicaDluga[i] + tablicaKrotka[i - roznicaDlugosci];
                zwracanaTablica[i] = wynik + reszta;
                reszta = 0;
                if (wynik > 9) {
                    reszta = 1;
                    zwracanaTablica[i] -= 10;
                }
            } else {
                zwracanaTablica[i] = tablicaDluga[i] + reszta;
            }
        }

        if (zwracanaTablica[0] > 9) {
            int[] tymczasowa = new int[zwracanaTablica.length + 1];
            tymczasowa[0] = 1;
            tymczasowa[1] = zwracanaTablica[0] - 10;
            for (int i = 2; i < tymczasowa.length; i++) {
                tymczasowa[i] = zwracanaTablica[i - 1];
            }
            zwracanaTablica = tymczasowa;
        }
        return zwracanaTablica;
    }

    static int[][] zadanie67(int dlugosc, int szerokosc) {
        int[][] duzaTablica = new int[dlugosc][szerokosc];

        Random losowacz = new Random();
        for (int i = 0; i < dlugosc; i++) {
            for (int j = 0; j < szerokosc; j++) {
                duzaTablica[i][j] = losowacz.nextInt(10);
            }
        }
        return duzaTablica;
    }

//    static boolean wyswietlacz(boolean[][] tablicaDoWyswietlenia) {
//        for (int i = 0; i < tablicaDoWyswietlenia.length; i++) {
//            for (int j = 0; j < tablicaDoWyswietlenia[0].length; j++) {
//                System.out.print(tablicaDoWyswietlenia[i][j] + " ");
//            }
//            System.out.println();
//        }
//    }

    static int[] zadanie68(int[][] tablica) {
        int[] nowaTablica = new int[tablica[0].length];
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica[0].length; j++) {
                nowaTablica[j] += tablica[i][j];
            }
        }
        return nowaTablica;
    }

    static boolean[][] zadanie69(int wysokosc, int szerokosc) {
        boolean[][] plansza = new boolean[wysokosc][szerokosc];
        int zageszczenie = 4;
        int liczbaBomb = wysokosc * szerokosc / zageszczenie;
        Random random = new Random();

        for (int i = 0; i < liczbaBomb; i++) {
            int x = random.nextInt(wysokosc);
            int y = random.nextInt(szerokosc);

            plansza[x][y] = true;

        }
        return plansza;

    }

    static int[] zdanieDodatkowe1(int rozmiar) { //Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną kolejnymi wartościami
        int[] tablica = new int[rozmiar];
        for (int i = 0; i < rozmiar; i++) {
            tablica[i] += i;
        }
        return tablica;
    }

    static int zadanieDodatkowe2(int[] tablica, int indeks) {  //Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę. Metoda ma zwrócić *INDEKS* szukanego elementu. Gdy dana liczba nie będzie znajdować się w tablicy, metoda powinna zwrócić wartość `-1`.
        for (int i = 0; i < tablica.length; i++) {

            if (tablica[i] != indeks) {
                continue;
            } else {
                return indeks;
            }
        }
        return -1;
    }

//    static int[] zadanieDodatkowe3(int[] tablica) {  //Utwórz metodę, która jako parametr przyjmuje tablicę i *zwraca nową tablicę* z liczbami w odwrotnej kolejności.
//        int[] odwrotnaTablica = new int[tablica.length];
//
//        for (int i = 0; i < tablica.length/2; i++) {
//            int x = tablica[i];
//            tablica.length[i]=tablica[tablica.length-i-1];
//            tablica[tablica.length-i-1]=x;
//        }
//        return odwrotnaTablica;
//    }

    static int[] zadanieDodatkowe4(int [] tablica, int poczatek, int koniec){ //Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz początek i koniec przedziału. Metoda ma zwrócić nową tablicę, która będzie zawierać elementy z wybranego przedziału.
        int [] nowaTablica = new int[tablica.length];

        for (int i = poczatek; i <= koniec; i++) {
            nowaTablica[i]+=i;
        }
        return nowaTablica;

    }

}
