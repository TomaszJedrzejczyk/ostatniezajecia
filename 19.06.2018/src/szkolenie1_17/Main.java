package szkolenie1_17;

public class Main {

    public static void main(String[] args) {

        myname();
        name("bartus");
        name("cycolina");
        dane("bogdan", 3938);
        dane("arkadiusz", 16);
        metod();
        math(2, 1, 3);

    }

    static void myname() {
        System.out.println("Tomaszek");
    }

    static void name(String imie) {
        System.out.println("witaj " + imie);
    }

    static void dane(String name, int age) {
        System.out.println("witaj " + name + " masz " + age);
        System.out.printf("witaj,%s masz %s lat\n", name, age); //%s zastepuje jakis string, za calym napisem okreslam to co ma sie w nich znajdowac pokolei
    }

    static void metod() {
        System.out.println(1 + 1 + "1"); //21
        System.out.println("1" + 1 + 1); //111
    }

    static void math(int a, int b, int c) {
        System.out.println(a + b + c);
    }
}

