package grupaSDA;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

//        petlaFor(5);
//
//        petlaWhile(5);
//
//        petlaDoWhile(5);

//   int[] tab = new int[]{1, 2, -2, 4, 7, -8};

//
//        System.out.println(zadanie58(tab, 1, 3));

//        zadanie59();

//        int[] wspak = zadanie60(new int[]{1, 7, 2, -4});
//        System.out.print(Arrays.toString(wspak));

//        int[] tab = zadanie61(new int[]{1, 2, -7, 8}, 3, 77);
//        System.out.println(Arrays.toString(tab));

        zadanie63();

    }

    static void petlaFor(int liczba) {
        for (int i = 0; i <= liczba; i++) {
            System.out.print(i + ", ");
        }
        System.out.println("\b\b");
    }

    static void petlaWhile(int liczba) {
        int i = 0;
        while (i <= liczba) {
            System.out.print(i + ", ");
            i++;
        }
        System.out.println("\b\b");
    }

    static void petlaDoWhile(int liczba) {
        int i = 0;
        do {
            System.out.print(i + ", ");
            i++;
        } while (i <= liczba);
        System.out.println("\b\b");
    }

    static int zadanie58(int[] array, int min, int max) {
        int x = 0;
        for (int i = min; i <= max; i++) {
            x += array[i];
        }
        return x;
    }

    static void zadanie59() {
        Scanner abc = new Scanner(System.in);
        int suma = 0;
        while (true) {
            System.out.print("podaj liczbe: ");
            int save = abc.nextInt();
            if (save < 0) {
                break;
            }
            suma += save;
        }
        System.out.println("suma podanych liczb to: " + suma);
    }

    static int[] zadanie60(int[] array) {
        int[] nowaTablica = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            nowaTablica[i] = array[nowaTablica.length - i - 1];
        }
        return nowaTablica;
    }

    static int[] zadanie61(int[] array, int pozycja, int nowyElement) {
        int[] nowaTablica = new int[array.length + 1];
        for (int i = 0; i <= pozycja; i++) {
            nowaTablica[i] = array[i];
        }
        nowaTablica[pozycja] = nowyElement;
        for (int i = pozycja + 1; i < nowaTablica.length; i++) {
            nowaTablica[i] = array[i - 1];
        }
        return nowaTablica;
    }

    static void zadanie63() {
        Scanner scan = new Scanner(System.in);
        int rozmiar;


        for (; ; ) {
            System.out.print("podaj rozmiar tablicy: ");
            rozmiar = scan.nextInt();
            if (rozmiar > 0) {
                break;
            }
            System.out.println("liczba jest ujemna, rozmiar musi byc dodatni");
        }

        int[] tablica = new int[rozmiar];
        int suma = 0;
        for (int i = 0; i < rozmiar; i++) {
            System.out.printf("Liczba [%s] :", i + 1);
            int wartosc = scan.nextInt();
            tablica[i] = wartosc;
            suma += wartosc;
        }
        System.out.println("podana tablica: " + Arrays.toString(tablica));
        System.out.println("suma elementow wynosi: " + suma);
    }

    static int[] zadanie63(int[] tablicaDuza) {
        int[] tablicaMala = new int[]{0, 0};

        for (int i = 0; i < tablicaDuza.length; i++) {
            if (tablicaDuza[i] > tablicaMala[0] && tablicaDuza[i] % 2 == 0) {
                tablicaMala[0] = tablicaDuza[i];
            } else if (tablicaDuza[i] < tablicaMala[1] && tablicaDuza[i] % 2 != 0) {
                tablicaMala[1] = tablicaMala[i];
            }
            return tablicaMala;
        }
    }

}



