package grupaSDA.zadanie93;

public class Osoba {
    private String imie;
    private int wiek;

    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    @Override
    public String toString() {
        return String.format("nazywam sie %s i mam %s lat", imie, wiek);
    }
}
