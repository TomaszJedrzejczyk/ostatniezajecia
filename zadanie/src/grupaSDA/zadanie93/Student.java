package grupaSDA.zadanie93;

public class Student extends Osoba {

    private String uczelnia;
    private int rokStudiow;

    public Student(String imie, int wiek, String uczelnia, int rokStudiow) {
        super(imie, wiek);
        this.uczelnia = uczelnia;
        this.rokStudiow = rokStudiow;
    }

    @Override
    public String toString() {
        return super.toString()+String.format("Studjuje na %s na %s",uczelnia,rokStudiow);
    }
}
