package grupaSDA.zadanie93;

public class Wykladowca extends Osoba{
    private int stopien;
    private Uczelnia placowka;

    public Wykladowca(String imie, int wiek, int stopien, Uczelnia placowka) {
        super(imie, wiek);
        this.stopien = stopien;
        this.placowka = placowka;
    }
}
