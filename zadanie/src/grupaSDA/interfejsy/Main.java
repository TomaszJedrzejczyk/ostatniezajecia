package grupaSDA.interfejsy;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Kolo kolo = new Kolo(5);
        Kwadrat kwadrat = new Kwadrat(7);
        Prostokat prostokat = new Prostokat(3, 4);

        System.out.println(kolo.policzPole());
        System.out.println(kwadrat.policzObjetosc(3));
        System.out.println(prostokat.policzObwod());

        Ksztalt [] tablica = new Ksztalt[]{kolo,kwadrat,prostokat};
        wyswietl(tablica);


    }

    private static void wyswietl(Ksztalt[] tablica) {
        String wzorzec = "Na pozycji %s. jest %s.";
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] instanceof Kwadrat){
                System.out.printf(wzorzec,i,"kwadrat");
            }if (tablica[i] instanceof Kolo){
                System.out.printf(wzorzec,i,"kolo");
            }if (tablica[i] instanceof Prostokat){
                System.out.printf(wzorzec,i,"prostokat");
            }
            System.out.println();
        }
    }


}
