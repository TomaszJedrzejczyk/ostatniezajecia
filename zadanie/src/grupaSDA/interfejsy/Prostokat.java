package grupaSDA.interfejsy;

public class Prostokat extends Ksztalt implements Obliczenia {
    int dlugosc;
    int szerokosc;

    public Prostokat(int dlugosc, int szerokosc) {
        this.dlugosc = dlugosc;
        this.szerokosc = szerokosc;
    }

    @Override
    public int policzPole() {
        return dlugosc * szerokosc;
    }

    @Override
    public int policzObwod() {
        return 2 * dlugosc + 2 * szerokosc;
    }

    @Override
    public int policzObjetosc(int wysokosc) {
        return policzPole() * wysokosc;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
