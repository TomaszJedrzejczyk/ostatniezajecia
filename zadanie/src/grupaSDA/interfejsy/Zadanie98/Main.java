package grupaSDA.interfejsy.Zadanie98;

public class Main {

    public static void main(String[] args) {
        Osoba osoba = new Osoba("Tobiasz","Boberek", 23);
        System.out.println(osoba.wyswietlDane());

        Pracownik pracownik = new Pracownik("Bogdan", "Strzelba", 57);
        System.out.println(pracownik.wyswietlDane());
    }
}
