package grupaSDA.interfejsy.Zadanie98;

public class Osoba implements PrzedstawSie {
   private String imie;
   private String nazwisko;
   private int wiek;

    public Osoba(String imie, String nazwisko, int wiek) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
    }

    @Override
    public String wyswietlDane() {
        return String.format("Nazywam sie %s i mam na nazwisko %s i mam %s",imie,nazwisko,wiek);
    }
}
