package grupaSDA.interfejsy.Zadanie98;

import grupaSDA.interfejsy.Zadanie98.Osoba;

public class Pracownik extends Osoba {
    private int pensja;

    public Pracownik(String imie, String nazwisko, int wiek) {
        super(imie, nazwisko, wiek);
    }

    public void setPensja(int pensja) {
        this.pensja = pensja;
    }


}
