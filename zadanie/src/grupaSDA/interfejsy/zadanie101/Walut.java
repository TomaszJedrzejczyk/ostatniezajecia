package grupaSDA.interfejsy.zadanie101;

public class Walut {
    private double kursKupna;
    private double kursSprzedarzy;
    private  Waluty pieniadz;


    public Walut(double kursKupna, double kursSprzedarzy, Waluty pieniadz) {
        this.kursKupna = kursKupna;
        this.kursSprzedarzy = kursSprzedarzy;
        this.pieniadz = pieniadz;
    }

    @Override
    public String toString() {
        return "Walut{" +
                "kursKupna=" + kursKupna +
                ", kursSprzedarzy=" + kursSprzedarzy +
                ", pieniadz=" + pieniadz.getOpis() +
                '}';
    }
}
