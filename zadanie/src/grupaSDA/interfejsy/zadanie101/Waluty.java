package grupaSDA.interfejsy.zadanie101;

public enum  Waluty {

    USD("Dolar"), EUR("Euro"), PLN("Polski złoty");
    private String opis;

    Waluty(String opis){
        this.opis=opis;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
}
