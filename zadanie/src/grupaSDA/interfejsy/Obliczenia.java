package grupaSDA.interfejsy;

public interface Obliczenia {

     int policzPole();
     int policzObwod();
     int policzObjetosc(int wysokosc);
}
