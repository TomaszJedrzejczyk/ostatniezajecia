package grupaSDA.interfejsy.zadanie100;

public class Main {
    public static void main(String[] args) {

        Samochod auto1 = new Samochod("Mazda",120,Kolor.GREEN);
        Samochod auto2 = new Samochod("Maluch",20,Kolor.GREY);
        Samochod auto3 = new Samochod("ferari",1120,Kolor.BLUE);

        System.out.println(auto1);
        System.out.println(auto2);
        System.out.println(auto3);
    }
}
