package grupaSDA.interfejsy.zadanie100;

import grupaSDA.interfejsy.Kolo;

public class Samochod {
    private String nazwa;
    private int moc;
    private Kolor barwa;


    public Samochod(String nazwa, int moc, Kolor barwa) {
        this.nazwa = nazwa;
        this.moc = moc;
        this.barwa = barwa;
    }

    @Override
    public String toString() {
        return String.format("Marka: %s\n Moc: %s\n Kolor: %s",nazwa,moc,barwa);
    }
}
