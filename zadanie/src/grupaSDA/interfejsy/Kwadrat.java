package grupaSDA.interfejsy;

public class Kwadrat extends Ksztalt implements Obliczenia {
    int dlugosc;

    public Kwadrat(int dlugosc) {
        this.dlugosc = dlugosc;
    }

    @Override
    public int policzPole() {
        return dlugosc * dlugosc;
    }

    @Override
    public int policzObwod() {
        return dlugosc * 4;
    }

    @Override
    public int policzObjetosc(int wysokosc) {
        return policzPole() * wysokosc;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
