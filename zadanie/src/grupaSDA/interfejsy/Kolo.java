package grupaSDA.interfejsy;

public class Kolo extends Ksztalt implements Obliczenia {
    int promien;

    public Kolo(int promien) {
        this.promien = promien;
    }

    @Override
    public int policzPole() {
        return (int) (Math.PI * promien * promien);
    }

    @Override
    public int policzObwod() {
        return (int) (2 * Math.PI * promien);
    }

    @Override
    public int policzObjetosc(int wysokosc) {
        return (policzPole() * wysokosc);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
