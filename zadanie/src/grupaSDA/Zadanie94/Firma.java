package grupaSDA.Zadanie94;

public class Firma {
    private String nazwaFirmy;
    private int rokZalozenia;
    private Pracownik[] listaPracownnikow = new Pracownik[10];

    public Firma(String nazwaFirmy, int rokZalozenia) {
        this.nazwaFirmy = nazwaFirmy;
        this.rokZalozenia = rokZalozenia;
    }

    public void wyswietlPracownikow() {
        System.out.println("Lista pracownikow: ");
        for (Pracownik pracownik : listaPracownnikow) {
            System.out.println(pracownik);
        }
    }

    public void skrocownaWersjaPracownika() { //netoda skocone wyswitlanie ktora nie otrzymuje zadniego parametru i nic nie zwraca
        System.out.println("Lista pracownikow: ");
        for (Pracownik pracownik : listaPracownnikow) {
            if (pracownik != null) {
                System.out.print(pracownik);
            } else {
                break;
            }
            System.out.println(", ");
        }
        System.out.println("\b\b");
    }

    public Pracownik zwrocPracownikaZPensjaMax() {
        Pracownik najbogatszy = listaPracownnikow[0];
        for (Pracownik obecniePrzegladanyPracownik : listaPracownnikow) {
            if (obecniePrzegladanyPracownik == null) {
                break;
            }
            if (obecniePrzegladanyPracownik.getPensja() > najbogatszy.getPensja()) {
                najbogatszy = obecniePrzegladanyPracownik;
            }
        }
        return najbogatszy;
    }

    public Pracownik najstarszyPracownik() {
        Pracownik najstarszy = listaPracownnikow[0];
        for (Pracownik przegladanyPracownik : listaPracownnikow) {
            if (przegladanyPracownik == null) {
                break;
            }
            if (przegladanyPracownik.getWiek() > najstarszy.getWiek()) {
                najstarszy = przegladanyPracownik;
            }
        }
        return najstarszy;
    }

    public int liczbabaPracownikowStarszychNiz(int minimalnyWiek) {
        int zwracanaIlosc = 0;
        for (Pracownik sprawdzanyPracownik : listaPracownnikow) {
            if (sprawdzanyPracownik == null) {
                break;
            }
            if (sprawdzanyPracownik.getWiek() > minimalnyWiek) {
                zwracanaIlosc++;
            }
        }
        return zwracanaIlosc;
    }

    public void dodajPracownika(Pracownik nowy) {
        for (int i = 0; i < listaPracownnikow.length; i++) {
            if (listaPracownnikow[i] == null) {
                listaPracownnikow[i] = nowy;
                return;
            }
        }
        System.out.println("nie stac cie na wiecej pracownikow");
    }

    void przyznajPodwyzkeProcentowa(int podwyzka) {
        for (Pracownik pracownik : listaPracownnikow) {
            if (pracownik == null) {
                break;
            }
            pracownik.setPensja((int) (pracownik.getPensja() * (1 + podwyzka)));
        }
    }

    void przyznajPodwyzkeKwota(int kwotaPodwyzki){
        for (Pracownik pracownik : listaPracownnikow) {
            if (pracownik==null){
                break;
            }
            pracownik.setPensja(pracownik.getPensja()+kwotaPodwyzki);
        }
    }
}
