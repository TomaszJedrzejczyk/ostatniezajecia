package grupaSDA.Zadanie94;

public class Main {

    public static void main(String[] args) {

        Firma firma = new Firma("siajomi", 2010);

        Pracownik uzytkownik = new Pracownik("Grzesiu", "Banaszczyk", false);
        firma.dodajPracownika(uzytkownik);
        uzytkownik.setPensja(4000);
        uzytkownik.setWiek(25);

        Pracownik kierownik = new Pracownik("Andrzej", "Sasin", false);
        firma.dodajPracownika(kierownik);
        kierownik.setPensja(8000);
        kierownik.setWiek(19);

        Pracownik pracownik = new Pracownik("Gustaw", "Jajca", true);
        firma.dodajPracownika(pracownik);
        pracownik.setPensja(1500);
        pracownik.setWiek(56);

        Pracownik zona = new Pracownik("Svietlana", "Kszaczenko", true);
        firma.dodajPracownika(zona);
        zona.setPensja(13);
        zona.setWiek(18);


        firma.skrocownaWersjaPracownika();
        System.out.println(firma.zwrocPracownikaZPensjaMax());
        System.out.println(firma.najstarszyPracownik());
        System.out.println(firma.liczbabaPracownikowStarszychNiz(30));


    }
}
