package grupaSDA.Zadanie94;

public class Pracownik {

    private String imie;
    private String drugieImie;
    private String nazwisko;
    private int wiek;
    private boolean czyKobieta;
    private int pensja;
    private String adres;

    public Pracownik(String imie, String nazwisko, boolean czyKobieta) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.czyKobieta = czyKobieta;
    }

    public String getImie() {
        return imie;
    }

    public String getDrugieImie() {
        return drugieImie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getWiek() {
        return wiek;
    }

    public boolean isCzyKobieta() {
        return czyKobieta;
    }

    public int getPensja() {
        return pensja;
    }

    public String getAdres() {
        return adres;
    }

    public void setDrugieImie(String drugieImie) {
        this.drugieImie = drugieImie;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public void setPensja(int pensja) {
        this.pensja = pensja;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String nazwaUzytkownika() {
        String zwracaneZdanie = imie;
        if (drugieImie != null) {
            zwracaneZdanie += " " + drugieImie;
        }
        zwracaneZdanie += " " + nazwisko + " " + "[" +
                (czyKobieta ? "K" : "M") + "] ";
        if (wiek>0) {
            zwracaneZdanie += "i mam " + ileDoEmerytury() + " lat do emerytury";
        }
        return zwracaneZdanie;
    }

    public int ileDoEmerytury() {
        return (czyKobieta ? 60 : 65) - wiek;
    }

    @Override
    public String toString() {
        return nazwaUzytkownika();
    }
}
