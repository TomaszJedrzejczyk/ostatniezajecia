package grupaSDA;

public class Mechanizm {
    static int suma(int a, int... elemety) {
        int wynik = a;
        for (int i = 0; i < elemety.length; i++) {
            wynik += elemety[i];
        }
        return wynik;
    }

    static String sum(String a, String... elementy) {
        StringBuilder wynik = new StringBuilder(a);
//        for (int i = 0; i < elementy.length; i++) {
//            wynik+=elementy[i]+" ";
//        }
        for (String i : elementy) {
            wynik.append(" ");
            wynik.append(i);
        }
        return wynik.toString();
    }

    static int zadanie91(boolean czyNajwieksza, int liczba, int... liczby) {

        int zwracanaLiczba = liczba;
        for (int i = 0; i < liczby.length; i++) {

            if (czyNajwieksza) {
                if (liczby[i] > zwracanaLiczba) {
                    zwracanaLiczba = liczby[i];
                }
            } else {
                if (liczby[i] < zwracanaLiczba) {
                    zwracanaLiczba = liczby[i];
                }
            }
        }
        return zwracanaLiczba;
    }
}
