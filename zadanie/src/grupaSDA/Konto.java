package grupaSDA;

public class Konto {

    private int stanKonta;
    private String wlascicielKonta;
    private static int numerKonta = 1;
    private int unikalnyNumer;


    public int getUnikalnyNumer() {
        return unikalnyNumer;
    }

    public int getStanKonta() {
        return stanKonta;
    }

    public Konto() {
        unikalnyNumer = numerKonta;
        numerKonta++;
    }

    int wplacanieNaKOnto(int wplata) {
        System.out.println("obecny stan konta: " + (stanKonta += wplata));

        return stanKonta;
    }

    int wyplacanieZKOnta(int wyplata) {
        if (wyplata > stanKonta) {
            System.out.println("nie masz tyle srodkow na koncie");
        } else {
            System.out.println("obecny stan konta: " + (stanKonta -= wyplata));
        }
        return stanKonta;
    }
}
