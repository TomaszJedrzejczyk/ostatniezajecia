package grupaSDA.zadanie108;

import java.lang.reflect.Array;
import java.util.*;
import java.util.zip.InflaterInputStream;

public class Main {

    public static void main(String[] args) {

        List<Integer> lista = new ArrayList<>(Arrays.asList(
                1,3,4,5,6,23,454,1,53));
        System.out.println(zadanie114(lista));


//        List<Integer> listaLiczb = wylosujLiczby();
////        System.out.println(zwrocSume(listaLiczb));
//        System.out.println(listaLiczb);
//        System.out.println(najwiekszaZListy(listaLiczb));
//        System.out.println(Collections.max(listaLiczb));
//        System.out.println(najwiekszaNajmniejsza(listaLiczb));

    }

    static List<Integer> wylosujLiczby() {        // petla ktora wypenia wartosciami losowymi
        Random random = new Random();
        List<Integer> nowaLista = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            nowaLista.add(random.nextInt(20));
        }
        return nowaLista;
    }

    static int zwrocSume(List<Integer> listaLiczb) {
        int suma = 0;

        for (int liczba : listaLiczb) {
            suma += liczba;
        }
        return suma;

    }

    static int najwiekszaZListy(List<Integer> listaLiczb) {
        int najwieksza = listaLiczb.get(0);

        for (int i = 1; i < listaLiczb.size(); i++) {
            if (listaLiczb.get(i) > najwieksza) {
                najwieksza = listaLiczb.get(i);
            }
        }
        return najwieksza;
    }

    static int najwiekszaZListy2(List<Integer> listaLiczb) {
        int najwieksza = listaLiczb.get(0);

        for (int liczba : listaLiczb) {
            if (najwieksza < liczba) {
                najwieksza = liczba;
            }
        }
        return najwieksza;
    }

//    static int najwiekszaNajmniejsza(List<Integer> listaLiczb) {
//        int max = Collections.max(listaLiczb);
//        int min = Collections.min(listaLiczb);
//        listaLiczb.add();
//        return max - min;
    }

//    public static List<Integer> zwaracaListePodanychLiczb() {
//        Scanner scanner = new Scanner(System.in);           //metody scannera szczytuja wartosci
//
//        List<Integer> listaLiczb = new ArrayList<>();
//        for (; ; ) {                                 //petla nieskonczonoci
//            System.out.println("Podaj liczbe: ");
//            int pobranaLiczba = scanner.nextInt();    //nextInt szczytujej podane wartosci  na konsoli
//            if (pobranaLiczba == -1) {
//                break;
//            }
//            listaLiczb.add(pobranaLiczba);
//        }
//        return listaLiczb;
//    }
//
//    static int countCmileyeyss(List<String> arr) {
//        int counter = 0;
//        String validSmilyFace = ":) ;) :-) :~) ;-) ;~) :D ;D :-D :~d ;~D ;~) ;-D";
//                String[]array = validSmilyFace.split(" ");
//        List<String> list = new ArrayList<String>(Arrays.asList(array));
//        for (int i = 0; i < arr.size(); i++) {
//            if (list.contains(arr.get(i))){
//                counter++;
//            }
//        }
//        return counter;
//    }

    static List<List<Integer>>zadanie114(List<Integer>someList){
        List<Integer> evens = new ArrayList<>();
        List<Integer> odds = new ArrayList<>();

        for (Integer aSomeList : someList) {
            if (aSomeList % 2 == 0) {
                evens.add(aSomeList);
            }else {
                odds.add(aSomeList);
            }
        }
        return new ArrayList<>(Arrays.asList(evens,odds));
    }
}
