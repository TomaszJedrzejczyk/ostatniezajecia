package grupaSDA;

import java.util.Arrays;

public class PracaDomowa {

    public static void main(String[] args) {

        int[] tablica = new int[]{2, 9, 13, 10, 5, 2, 9, 5};

        System.out.println(Arrays.toString(tablica));
        System.out.println(maxTriSum(tablica));

    }


    static int maxTriSum(int[] numbers) {
        int max1 = numbers[0];
        int max2 = numbers[1];
        int max3 = numbers[2];

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > max3 && numbers[i] < max2) {
                max3 = numbers[i];
            }
            if (numbers[i] > max2 && numbers[i] < max1) {
                max3 = max2;
                max2 = numbers[i];
            }
            if (numbers[i] > max1) {
                max3 = max2;
                max2 = max1;
                max1 = numbers[i];
            }
        }

        return max1 + max2 + max3;
    }


}
