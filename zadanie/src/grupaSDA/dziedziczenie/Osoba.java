package grupaSDA.dziedziczenie;

public class Osoba {
    protected String imie;
    protected int wiek;

    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    @Override
    public String toString() {
        return String.format("Masz na imie %s, masz %s lat.", imie,wiek);
    }
}
