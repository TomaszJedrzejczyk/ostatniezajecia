package grupaSDA.dziedziczenie;

public class Kierownik extends Pracownik {

    private int premia;

    public Kierownik(String imie, int wiek, int pensja, int premia) {
        super(imie, wiek, pensja);
        this.premia = premia;
    }

    @Override
    public String toString() {
        return super.toString() + " Twoja premia wynosi: " + premia + ".";
    }

    @Override
    int policzKosztPracownika() {
        return super.policzKosztPracownika() + premia * 4;
    }
}
