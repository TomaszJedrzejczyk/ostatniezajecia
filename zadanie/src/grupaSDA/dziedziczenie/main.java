package grupaSDA.dziedziczenie;

public class main {

    public static void main(String[] args) {
        Osoba osoba = new Osoba("Bogdan", 30);
        System.out.println(osoba);
        Pracownik pracownik = new Pracownik("Roman",25,15000);
        System.out.println(pracownik);
        Kierownik kierownik =new Kierownik("Zenek",66,20000,1000);
        System.out.println(kierownik);

        System.out.println(kierownik.policzKosztPracownika());
    }
}
