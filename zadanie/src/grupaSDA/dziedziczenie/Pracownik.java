package grupaSDA.dziedziczenie;

public class Pracownik extends Osoba {

    protected int pensja;

    public Pracownik(String imie, int wiek, int pensja) {
        super(imie, wiek); //super imporutje dane wiek i imie z poprzedniej klasy
        this.pensja = pensja;
    }

    @Override
    public String toString() {
        return super.toString() + " Zarabiasz " + pensja + ".";
    }

    int policzKosztPracownika() {
        return 12 * pensja;
    }
}
