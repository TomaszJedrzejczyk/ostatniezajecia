package grupaSDA.zadanie102;

public class NumerTelefonu {
    private int numer;
    private Kierunkowy kierunkowy;

    public NumerTelefonu(int numer, Kierunkowy kierunkowy) {
        this.numer = numer;
        this.kierunkowy = kierunkowy;
    }

    @Override
    public String toString() {
        return kierunkowy.jakoTekst() + " " + numer;
    }
}
