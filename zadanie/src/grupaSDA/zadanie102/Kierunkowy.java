package grupaSDA.zadanie102;

public enum Kierunkowy {
    POLSKA(48), NIEMCY(49), ROSJA(7);
    private int poleNumer;

    Kierunkowy(int poleNumer) {
        this.poleNumer = poleNumer;
    }

    String jakoTekst(){
        return "+" + poleNumer;

    }
    int jakoLiczba(){
        return poleNumer;

    }

}

