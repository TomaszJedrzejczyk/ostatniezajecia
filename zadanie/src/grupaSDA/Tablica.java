package grupaSDA;

public class Tablica {

    private int[] tablica;

    public Tablica() {
//        tablica = new int[1];
        this(1);
    }

    public Tablica(int rozmiar) {
        tablica = new int[rozmiar];
    }

    int rozmiarTablicy() {
        return tablica.length;
    }

    void wyswietlanieTablicy() {
//        for (int i = 0; i < tablica.length; i++) {
//            System.out.print(tablica[i]+", ");
//        }
        for (int liczba : tablica) {
            System.out.print(liczba + ", ");
        }
        System.out.println();
    }

    void add(int nowaWartosc) {
        przepisywacz(1);
        tablica[tablica.length - 1] = nowaWartosc;

    }


    void przepisywacz(int liczbaDoPowiekszenia) {
        int[] nowatab = new int[tablica.length + liczbaDoPowiekszenia];

        for (int i = 0; i < tablica.length; i++) {
            nowatab[i] = tablica[i];
        }
        tablica = nowatab;
    }

    int najwiekszaWartosc() {
        int max = tablica[0];
        for (int i = 1; i < tablica.length; i++) {
            if (tablica[i] > max) {
                max = tablica[i];
            }
        }
        return max;
    }

    void wstawienieElementuZPrzodu(int nowaWartosc) {
        int[] nowatablica = new int[tablica.length + 1];
        nowatablica[0] = nowaWartosc;
        for (int i = 0; i < tablica.length; i++) {
            nowatablica[i + 1] = tablica[i];
        }
        tablica = nowatablica;
    }

    void wstawianieElementuPodWybranymIndesem(int index, int pozycja) {
        int[] nowaTablica = new int[tablica.length + 1];
        nowaTablica[pozycja] = index;

        for (int i = 0; i < pozycja; i++) {
            nowaTablica[i] = tablica[i];
        }
        for (int i = pozycja; i < tablica.length; i++) {
            nowaTablica[i+1] = tablica[i];
        }
        tablica=nowaTablica;

    }

    void  usuwanieElementuPodWybranymIndexem(int pozycja){
        int [] nowaTablica = new int[tablica.length-1];

        for (int i = 0; i < pozycja; i++) {
            nowaTablica[i]=tablica[i];
        }
        for (int i = pozycja; i < nowaTablica.length; i++) {
            nowaTablica[i]=tablica[i+1];
        }
        tablica=nowaTablica;
    }


}
