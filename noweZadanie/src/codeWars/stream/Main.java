package codeWars.stream;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main { //strims and lambdas

    static List<Osoba> osobaList = Arrays.asList(
            new Osoba("Grazynka", 56),
            new Osoba("Janusz", 72),
            new Osoba("Mariolka", 43),
            new Osoba("Bozenka", 58)
    );

    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
//        method4();
//        method5();
//        method6();
//        method7();
//        method8();
//        method9();
//        method10();
//        method11();
//        method12();
//        method13();
//        method14();
        method15();
    }

    public int GetSum(int a, int b) {
        if (a==b){
            System.out.println(a+" Since both are same");
        }
        return  IntStream.of(a,b).sum();
    }

    public static String[] removeRotten(String[] fruitBasket) {
        if (fruitBasket == null) {
            return new String[]{""};
        }
        return Arrays.stream(fruitBasket)
                .map(f -> f.replaceAll("rotten", ""))
                .map(String::toUpperCase).toArray(String[]::new);
    }

    public static String bumps(final String road) {
        return road.chars().filter(n -> n == 'n').count() > 15 ? "Car Dead" : "Woohoo!";
    }

    private static void method15() {
        List<Osoba> list = osobaList.stream()
                .filter(osoba -> osoba.getImie().startsWith("G"))
                .collect(Collectors.toList());//odrazu przypisuje wynik do listy

        System.out.println(list);
        list.forEach(System.out::println);
    }

    private static void method14() {
        Stream.of("styczeń", "luty", "marzec", "kwiecień")
                .filter(miesiac -> {
                    System.out.println("filter: " + miesiac);
                    return miesiac.endsWith("ń"); //filtruje po miesiacach ktore zawieraja ń
                })
                .map(miesiac -> {
                    System.out.println("map: " + miesiac);
                    return miesiac.toUpperCase(); // przefiltrowane miesiace sa mapowane i podnoszone do duzej litery
                })
                .forEach(miesiac -> {
                    System.out.println(miesiac);    //wyswietla miesiac
                });
    }

    private static void method13() {
        Stream.of("a1", "a2", "a3", "a4")//przetwarzanie elementu
                .map(n -> n.substring(1))   //ucinanie znakow od 1 do konca
                .mapToInt(Integer::parseInt)    //przerabianie napisow na liczby
                .max()
                .ifPresent(System.out::println);
    }

    private static void method12() {
        Arrays.asList("ala", "kot", "ma", "mama")
                .stream()
                .filter(n -> n.endsWith("ma"))// z kilku opcji wybiera nasz poprawne, najpierw filter potem reszte operacji
                .map(String::toUpperCase)// przetwaza dane (w tym przypdaku wyciaga przefiltrowane i podnosi je do upperCase)
                .forEach(System.out::println);
    }

    private static void method11() {
        Arrays.asList("lista", "gool", "zawodnik")
                .stream()
                .filter(napis -> napis.contains("a"))//filtruje z kazdego napisu i sprawdza czy napisy maja w sobie "a"
                .findFirst()// zostanie zwrocona pierwsza po spelnieniu warunkow
                .ifPresent(System.out::println);    //jezeli jest to: wyswietl
    }

    private static void method10() {
        List<String> list = Arrays.asList("wtorek", "piatek", "", "czwartek");
        list = list.stream()
                .filter(napis -> {
                    System.out.println("obecny napis: " + napis);
                    if (napis.isEmpty()) {
                        return false;
                    }
                    return true;
                })
                .collect(Collectors.toList()); //zwracanie do listy

        System.out.println(list);
    }

    private static void method9() {
        List<Integer> list = Arrays.asList(4, 1, 3, 2, 5);
        IntSummaryStatistics statistics = list.stream()
                .mapToInt(n -> n)
                .summaryStatistics();

        System.out.println("srednia: " + statistics.getAverage());
        System.out.println("maksymalna: " + statistics.getMax());
        System.out.println("minimalna: " + statistics.getMin());
        System.out.println("suma: " + statistics.getSum());
    }

    private static void method8() {
        double average = IntStream
                .iterate(1, n -> n + 3) //wybieraz co 3 cyfre
                .limit(3) // limit 3
                .average() // oblicza srednia
                .orElse(000);// jezeli mu sie nie uda to zwraza 000
        System.out.println(average);
    }

    private static void method7() {
        int sum = IntStream
                .iterate(1, n -> n + 2) //od 1 co druga wartosc, drugie to wyrazenie lambda
                .limit(5)
                .sum();
        System.out.println(sum);
    }

    private static void method6() {
        int[] array = new int[]{};

        Arrays.stream(array)
                .average()  //liczy srednia
                .ifPresent(System.out::println);    //jezeli wartosci sa to je oblicza jak nie to nie

        double average = Arrays.stream(array)
                .average()
                .orElse(0); //jezeli nie zadziala wyrazenie wczesniej to zrobi podana przez nas rzecz
        System.out.println(average);
    }

    private static void method5() {
        Random random = new Random();
        List<Integer> integerList = random.ints(6, 10, 20) // podajemy ile ma wylosowac liczb, od jakiego do jakiego zakresu
                .boxed()    //przepakowywuje z int do Integare
                .collect(Collectors.toList());  // pakujemy wszystko do listy

        System.out.println(integerList);
    }

    private static void method4() {
        Random random = new Random();   //tworzymy obiekt klasy random
        random.ints()
                .limit(5)      // dajemy ograniczenie zakresu randoma do 5
                .forEach(n -> System.out.println(n));
    }

    private static void method3() {
        IntStream
                .rangeClosed(1, 10)
                .filter(number -> number % 2 == 0)   //podajemy wartosc logiczna, czy ma isc dalej czy nie
                .forEach(System.out::println);
    }

    private static void method2() {
        IntStream
                .rangeClosed(1, 10) //utworzy ciag znakow od 1 do 10
                .forEach(System.out::println); //tylko do metod jednoparametrowych, pamietaj o '::'!!!

    }

    private static void method1() {
        IntStream
                .range(1, 10)   //utworz ciag elementow od 1 do 9
                .forEach(n -> System.out.println(n));    //->co jest za strzalka ma sie wykonac, wyrazenie typu lambda

    }
}
