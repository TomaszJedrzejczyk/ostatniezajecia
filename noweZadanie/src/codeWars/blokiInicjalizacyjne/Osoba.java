package codeWars.blokiInicjalizacyjne;

public class Osoba {
    static int kod = 24;

    {
        System.out.println("Drugi blok inicjalizacyjny osoby");
    }

    {
        System.out.println("Pierwszy inicjalizacyjny osoby");
    }

    static {                                                    //najpierw wywoluje statyczne bloki inicjalizacyjne
        System.out.println("Statyczny blok inicjalizacyjny");
    }

    public Osoba(String imie) {
        System.out.println("Konstruktor jednoparametrowy osoby");
    }

    public Osoba() {
        System.out.println("Konstruktor osoby");
    }
}
