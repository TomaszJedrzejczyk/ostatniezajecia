package codeWars.blokiInicjalizacyjne;

public class Pracownik extends Osoba {

    /**
     * zawód naszego pracownika
     */
    public String zawod;

    {
        System.out.println("Blok inicjalizacyjny pracownika");
    }

    /**
     * @param zawod zawod mojego pracownika
     */
    public Pracownik(String zawod) {
        System.out.println("Jednoparametrowy konstruktor pracownika");
    }

    public Pracownik() {
        System.out.println("Konstruktor pracownika");
    }

    /**
     *
     * @param liczba1 liczba pierwsza
     * @param liczba2 liczba druga
     * @return suma przekazanych parametrow
     */
    static int method(int liczba1, int liczba2) {
        return liczba1 + liczba2;
    }
}
