package zadanka;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {

    }

    public static String countingSheep(int num) {
        return IntStream.rangeClosed(1, num)     //zakres od w ktorym bedziemy liczyc
                .mapToObj(number -> number + " sheep...")
                .collect(Collectors.joining()); //joining skleja wszystkie przekazane elementy ktore dostaje
    }

    public static int getAverage(int[] marks) {
        return (int) Arrays.stream(marks).average().orElse(0);
    }

    static int GetSum(int a, int b) {
        return IntStream
                .rangeClosed(Math.min(a, b), Math.max(a, b))
                .sum();
    }

    public static int[] extraPerfect(int number) {
        return IntStream
                .rangeClosed(1, number)
                .filter(n -> n % 2 != 0)
                .toArray();
    }

    public static String repeatStr(final int repeat, final String string) {
        return IntStream.rangeClosed(1, repeat)
                .mapToObj(n -> string)
                .collect(Collectors.joining());
    }

    public String spinWords(String sentence) {
        String[] tablica = sentence.split(" ");
        return Arrays.stream(tablica)
                .map(w ->{
                    if (w.length()>4){
                        StringBuffer slowo = new StringBuffer(w);
                        return slowo.reverse().toString();
                    } else {

                        return w;
                    }
                })
                .collect(Collectors.joining(" "));
    }

    public static List<String> myLanguages(final Map<String, Integer> results) {
        return results.entrySet()
                .stream()
                .filter(n->n.getValue()>=60)
                .sorted(Comparator.comparingInt(Map.Entry::getValue))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public static String twoSort(String[] s) {
        String[] split = Arrays.stream(s)
                .sorted()
                .findFirst()
                .orElse("")
                .split("");

        return Arrays.stream(split)
                .collect(Collectors.joining("***"));
    }
}
