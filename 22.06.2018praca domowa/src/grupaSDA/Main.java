package grupaSDA;

public class Main {

    public static void main(String[] args) {

        // System.out.println(fib(7));
//
//        System.out.println(primal(3));

//       metoda(3);
//
//        fib2(1);

//        zadanie46(9, 7, "k");

        zadanie47(2,6,8,5);
}

    static int fib(int num) {
        if (num == 0) {
            return 0;
        }
        if (num == 1) {
            return 1;
        } else {
            return fib(num - 1) + fib(num - 2);
        }
    }

    static int fib2(int liczba) {
        int liczba1 = 1;
        int liczba2 = liczba1;
        int liczba3;
        for (int i = 0; i < liczba; i++) {
            System.out.println(liczba2);
            liczba3 = liczba1;
            liczba1 += liczba2;
            liczba2 = liczba3;
        }
        return liczba1;
    }

    static boolean primal(int num) {
        if (num < 2) {
            return false;
        }
        for (int i = 2; i * i <= num; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    static void metoda(int liczba) {

        for (int i = 0; i <= liczba; i++) {
            System.out.printf("%s - %s liczba pierwsza\n",
                    i,
                    primal(i) ? "jest" : "nie jest");
        }
    }

    static void zadanie42(int num) {

        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= num; j++) {
                System.out.print(i * j + "\t");
            }
            System.out.println();
        }
    }

    static void zadanie43(int num) {
        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j);
            }
            System.out.println();
        }
    }

    static void zadanie44(int num) {

        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= num; j++) {
                if (j <= num - i) {
                    System.out.print(".");
                } else {
                    System.out.print(i);
                }
            }
            System.out.println();
        }
    }

    static void grazyna(int num) {
        for (int i = 1; i <= num; i++) {
            for (int j = 0; j <= num - i; j++) {
                System.out.print(".");
            }
            for (int j = num - i; j < num; j++) {
                System.out.print(i);
            }
            System.out.println();
        }
    }

    static void zadanie45(int num, int num2) {
        for (int i = 1; i <= num2; i++) {
            System.out.print(" ");
            for (int j = 0; j < i; j++) {
                System.out.print(num);
            }
        }

    }

    static void zadanie46(int szerokosc, int wysokosc, String znak) { //program rysuje kwadracik
        for (int i = 0; i < szerokosc; i++) {
            System.out.print(znak + " ");
        }
        System.out.println();
        for (int i = 0; i < wysokosc - 2; i++) {
            System.out.print(znak + " ");
            for (int j = 0; j < szerokosc - 2; j++) {
                System.out.print("  ");
            }
            System.out.println(znak);
        }
        for (int i = 0; i < szerokosc; i++) {
            System.out.print(znak + " ");
        }
    }

    static void zadanie47(int szerokosc, int szerokosc2, int wysokosc, int wysokosc2){
        for (int i = 0; i < wysokosc-wysokosc2; i++) {
            for (int j = 0; j < szerokosc; j++) {
                System.out.print("x ");
            }
            System.out.println();
        }
        for (int i = 0; i < wysokosc2; i++) {
            for (int j = 0; j < szerokosc2; j++) {
                System.out.print("x ");
            }
            System.out.println();
        }
    }
}


