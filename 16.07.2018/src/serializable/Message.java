package serializable;

import java.io.Serializable;


public class Message implements Serializable {
    private String title;
    private String content;
    private static final long serialVersionUID = 1;

    public Message(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return String.format("%s \n%s", title, content);
    }
}
