package serializable;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        Message message = new Message("hi", "hello");
        Message message2 = new Message("welcone", "greatings");
        saveFile(message);
        Message messagedoOdczytu = readFile();
        System.out.println(messagedoOdczytu);
    }

    private static Message readFile() {
        try(FileInputStream fileInputStream = new FileInputStream("pliki/plikwynikowyy");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)){
            return (Message) objectInputStream.readObject();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private static void saveFile(Message message) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("pliki/file.txt");
            ObjectOutputStream stream = new ObjectOutputStream(fileOutputStream);
            stream.writeObject(message);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
