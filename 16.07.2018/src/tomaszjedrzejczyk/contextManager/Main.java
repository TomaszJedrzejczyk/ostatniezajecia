package tomaszjedrzejczyk.contextManager;

public class Main {
    public static void main(String[] args) {
        try (ContextManager contextManager = new ContextManager()){  //mamy 2 metody pierwsza odSomething oraz ContextMenager
            contextManager.odSomething();
        }catch (Exception e){
            e.printStackTrace();    // 3 metody uruchamia sie o zakonczeniu pracy programu
        }
    }
}
