package tomaszjedrzejczyk.contextManager;

public class ContextManager implements AutoCloseable {
    public ContextManager(){
        System.out.println("Utworzono obiekt");
    }

    void odSomething(){
        System.out.println("Praca w toku");
    }

    @Override
    public void close(){        //close uruchamia sie samo po zakonczeniu pracy programu
        System.out.println("Obiekt zostal zamkniety");
    }
}
