package tomaszjedrzejczyk.zadaniaDomowe;

import java.awt.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {

    public static int countRectTriang(final Point[] points) {

        Set<Point> punkty = new HashSet<Point>(Arrays.asList(points));
        Point[] unikalnePunkty = punkty.toArray(new Point[0]);
        int counter = 0;
        for (int i = 0; i < unikalnePunkty.length; i++) {
            for (int j = i + 1; j < unikalnePunkty.length; j++) {
                for (int k = j + 1; k < unikalnePunkty.length; k++) {
                    if (czyTrojkatJestProstokatny(unikalnePunkty, i, j, k)){
                        counter++;
                    }
                }
            }
        }
        return counter;
    }

    private static boolean czyTrojkatJestProstokatny(Point[] unikalnePunkty, int i, int j, int k) {
        long a = dlugosc(unikalnePunkty, i, j);
        long b = dlugosc(unikalnePunkty, i, k);
        long c = dlugosc(unikalnePunkty, j, k);
        return a == b + c || b == c + a || c == b + a;
    }

    private static long dlugosc(Point[] t, int i, int j) {
        return (long) (Math.pow((t[i].getX() - t[j].getX()), 2) + Math.pow((t[i].getY() - t[j].getY()), 2));

    }
}
