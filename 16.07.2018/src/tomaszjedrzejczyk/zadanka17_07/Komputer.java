package tomaszjedrzejczyk.zadanka17_07;

public abstract class Komputer {
    String nazwa;
    int cena;

    public Komputer(String nazwa, int cena) {
        this.nazwa = nazwa;
        this.cena = cena;
    }

    @Override
    public String toString() {
        return String.format("%s kosztuje %s",nazwa,cena);
    }
}
