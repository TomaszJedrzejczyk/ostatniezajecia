package tomaszjedrzejczyk.zadanka17_07;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        KomputerStacjonarny komp = new KomputerStacjonarny("comodore", 50_000);
        KomputerStacjonarny komp2 = new KomputerStacjonarny("atari", 20_000);
        Laptop lapek = new Laptop("Xiaomi", 1_000_000, 15);
        Laptop lapek2 = new Laptop("HP", 400, 13);
        List<Komputer> ListKomputerow = new ArrayList<>(Arrays.asList(komp, lapek, lapek2, komp2));

        for (Komputer komputer : ListKomputerow) {
            if (komputer instanceof Laptop) {       //instancja danej klasy, czy dany obiekt jest instancja tej klasy
                Laptop innyLaptop = (Laptop) komputer;
                System.out.print(innyLaptop + " rozmiar ekrantu to ");
                System.out.println(innyLaptop.getRozmiarEkranu() + " cali");
            } else {
                System.out.println(komputer);
            }
        }


    }
}
