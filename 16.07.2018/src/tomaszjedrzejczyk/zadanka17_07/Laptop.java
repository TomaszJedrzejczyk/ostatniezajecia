package tomaszjedrzejczyk.zadanka17_07;

public class Laptop extends Komputer {
    private int rozmiarEkranu;

    public Laptop(String nazwa, int cena, int rozmiarEkranu) {
        super(nazwa, cena);
        this.rozmiarEkranu = rozmiarEkranu;
    }

    public int getRozmiarEkranu() {
        return rozmiarEkranu;
    }
}
