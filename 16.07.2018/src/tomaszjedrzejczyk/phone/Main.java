package tomaszjedrzejczyk.phone;

public class Main {
    public static void main(String[] args) {
        Telephone tel = new Telephone("s9","samsung",Kolor.CZARNY);
        Telephone tel2 = new Telephone("mi","xiaomi",Kolor.SREBRNY);
        Telephone tel3 = new Telephone("mi","xiaomi",Kolor.SREBRNY);

        System.out.println(tel.equals(tel2));
        System.out.println(tel2.equals(tel3));
        System.out.println(tel3.equals(tel));
    }
}
