package tomaszjedrzejczyk.phone;

public class Telephone {
    String nazwa;
    String marka;
    Kolor kolor;

    public Telephone(String nazwa, String marka, Kolor kolor) {
        this.nazwa = nazwa;
        this.marka = marka;
        this.kolor = kolor;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass()!=this.getClass()){
            return false;
        }
        Telephone telephone = (Telephone) obj;
        if (!nazwa.equals(telephone.nazwa)){
            return false;
        }
        if (!marka.equals(telephone.marka)){
            return false;
        }
        if (!kolor.equals(telephone.kolor)){
            return false;
        }
        return true;


    }
}

enum Kolor {
    CZARNY, BIALY, SREBRNY
}


