package tomaszjedrzejczyk.firmaJP;

import java.util.Comparator;

public class SortowaczPoImionach implements Comparator {


    @Override
    public int compare(Object o1, Object o2) {
        if (o1 == o2) {
            return 0;
        }
        Pracownik p1 = (Pracownik) o1;
        Pracownik p2 = (Pracownik) o2;

        return p1.getImie().compareTo(p2.getImie());

    }
}
