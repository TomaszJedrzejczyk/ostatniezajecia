package tomaszjedrzejczyk.firmaJP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Pracownik> list = new ArrayList<>(Arrays.asList(
                new Pracownik("Krzysiek",18),
                new Pracownik("Mateusz", 16),
                new Pracownik("Aleksander", 15)
        ));

        System.out.println(list);

        list.sort(new SortowaczPoImionach());
        System.out.println(list);

        list.sort(new SortowaczPoWieku());
        System.out.println(list);
    }
}
