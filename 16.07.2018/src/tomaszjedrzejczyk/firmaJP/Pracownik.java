package tomaszjedrzejczyk.firmaJP;

public class Pracownik {
    private String imie;
    private int wiek;

    public Pracownik(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    public String getImie() {
        return imie;
    }

    public int getWiek() {
        return wiek;
    }

    @Override
    public String toString() {
        return String.format("%s %s", imie, wiek);
    }
}
