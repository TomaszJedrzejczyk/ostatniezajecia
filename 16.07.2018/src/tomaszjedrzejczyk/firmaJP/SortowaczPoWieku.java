package tomaszjedrzejczyk.firmaJP;

import java.util.Comparator;

public class SortowaczPoWieku implements Comparator{
    @Override
    public int compare(Object o1, Object o2) {

        Pracownik p1 = (Pracownik) o1;
        Pracownik p2 = (Pracownik) o2;

        return p1.getWiek() - p2.getWiek();
    }
}
