package tomaszjedrzejczyk.liczbyZmiennoPrzecinkowe;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
//        method4();
//        method5();
        method6();


    }

    private static void method6() {
        BigDecimal bigDecimal = new BigDecimal("0.1");
        System.out.println(bigDecimal);

        BigDecimal bigDecimal2 = new BigDecimal(new char[]{'0','.','2'});
        System.out.println(bigDecimal2);

        double d = bigDecimal.add(bigDecimal2).doubleValue();
        System.out.println(d);
        System.out.printf("%.30f\n",d);

        BigDecimal bigDecimal3 = new BigDecimal(0.1);
        System.out.println(bigDecimal3);
    }

    private static void method5() {
        float f = 0.1f;
        double d = 0.1d;
        System.out.printf(" wartosc float: %.50f\n", f);
        System.out.printf("wartosc double: %.50f\n", d);
        System.out.println(0.10000000149011612000000000000000000000000000000000 == 0.1f);  // pomimo niewielkiej roznicy java pokazuje nam to jako 'prawda' poniewaz jest najblizsza prawdzi

    }

    private static void method4() {
        double suma = 0;
        for (double i = 0; i < 10; i++) {
            suma += 0.1;
        }
        System.out.println(suma);
        System.out.printf("%.2f", suma);                // dopiero po zaokragleniu pokazuje pelna liczbe inaczej 0.9999999 najblizsza interpretacje 1
    }

    private static void method3() {
        for (float i = 10F; i > 0; i -= 0.1f) {        // mozna wpisywac albo 'f' albo 'F' tak zadam z 'd' i 'D'
            System.out.println(i);
        }
    }

    private static void method2() {
        for (double i = 0; i < 1; i += 0.1D) {
            System.out.println(i);                  // tak samo jak z float nie komputer nie moze wyswietlic 0.7 a wswietla 0.79999999999
        }
    }

    private static void method1() {
        for (float i = 0; i < 1; i += 0.1F) {         //komputer nie potrafi zapisac 0.7 dlatego jest 0.7000005
            System.out.println(i);
        }
    }
}
