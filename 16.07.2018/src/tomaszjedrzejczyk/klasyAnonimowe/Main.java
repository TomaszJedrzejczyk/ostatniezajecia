package tomaszjedrzejczyk.klasyAnonimowe;

public class Main {
    public static void main(String[] args) {
        methodOne();
        System.out.println("*_*_*_*_*_*_*_*_*");
        methodTwo();
        System.out.println("*_*_*_*_*_*_*_*_*");
        methodThree();
        System.out.println("*_*_*_*_*_*_*_*_*");
        methodFour();



    }

    private static void methodFour() {
        new Komputer(){
            @Override
            protected void preper() {
                System.out.println("Bardzijee anonimowe sprawdzenie");
                super.preper();
            }
        }.run();

    }

    private static void methodThree() {
        Komputer komputerAnonym = new Komputer() {
            @Override
            protected void preper() {
                System.out.println("Anonimowe sprawdzanie...");
                super.preper();
            }
        };
        komputerAnonym.run();
    }

    private static void methodTwo() {
        MojKomputer mojKomputer = new MojKomputer();
        mojKomputer.run();
    }

    private static void methodOne() {
        Komputer komputer = new Komputer();
        komputer.run();
    }
}
