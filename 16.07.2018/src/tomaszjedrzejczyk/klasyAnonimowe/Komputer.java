package tomaszjedrzejczyk.klasyAnonimowe;

public class Komputer {

    void run() {
        preper();
        System.out.println("Komputer zostal uruchomiony!");
    }

    protected void preper() {
        System.out.println("Uruchamianie w toku...");
    }
}
