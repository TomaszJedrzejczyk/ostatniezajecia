package tomaszjedrzejczyk.rekurencja;

public class Main {
    public static void main(String[] args) {
//        method1(5);
//        System.out.println(method2("kajak"));
//        System.out.println(method3(1234));
        System.out.println(method4(21));

    }

    private static long method4(long number) {
        if (number == 1) {
            return 1;
        } else {
            return number * method4(number - 1);
        }

    }

    private static int method3(int number) {
        if (number == 0) {
            return 0;
        } else {
            int rest = number % 10;
            return rest + method3(number / 10);
        }
    }

    private static boolean method2(String wordToCheck) {    // polindrom, czy slowo czytane od przodu i od tylu jest takie samo
        if (wordToCheck.length() == 0 || wordToCheck.length() == 1) { //sprawdzamy czy dotrzemy do zadanego albo jednego zanku w stringu
            return true;
        }
        if (wordToCheck.charAt(0) == wordToCheck.charAt(wordToCheck.length() - 1)) {     //porownujemy skrajne znaki ze soba
            return method2(wordToCheck.substring(1, wordToCheck.length() - 1));    //zwracamy ponownie metode z przycinaniem koncowek stringa
        }
        return false;
    }

    private static void method1(int counter) {
        counter--;
        if (counter > 0) {
            System.out.println(counter);
            method1(counter);                           //<--- wykorzystanie rekurencji, counter jest zmniejszany o 1
        }
    }
}
