package tomaszjedrzejczyk.zadanie147;

import java.io.*;

public class Main {
    public static void main(String[] args) {
//        System.out.println(zadanie147("pliki/1", "pliki/2"));
        zadanie148("pliki/1","pliki/2");
    }

    //  Utwórz metodę która przyjmuje dwa parametry, które są ścieżkami do plików.
    // Metoda ma zwrócić informację czy podane pliki są takie same. (tzn. porównać linia po linii)

    static boolean zadanie147(String filePath, String secondFilePath) {
        String line = "";
        String secondLine = "";
        int counter = 0;
        try (FileReader fileReader = new FileReader(filePath);
             FileReader secondFileReader = new FileReader(secondFilePath);

             BufferedReader bufferedReader = new BufferedReader(fileReader);
             BufferedReader secondbufferedReader = new BufferedReader(secondFileReader)) {

            line = bufferedReader.readLine();
            secondLine = secondbufferedReader.readLine();

            while (line != null && secondLine != null) {
                if (!line.equals(secondLine)) {
                    System.out.println("Pliki maja " + counter + " wspolnych linii");
                    return false;
                }
                line = bufferedReader.readLine();
                secondLine = bufferedReader.readLine();
                counter++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Pliki maja " + counter + " wspolnych linii");
        return line == null && secondLine == null;
    }

    //Utwórz metodę która przyjmuje dwa parametry, które są ścieżkami do plików.
    //Metoda ma scalić pliki “zazębiając” linijki (zapisując wynik w trzecim pliku)

    static void zadanie148(String filePath, String secondfilePath) {

        try (FileReader fileReader = new FileReader(filePath);
             FileReader secondFileReader = new FileReader(secondfilePath);

             BufferedReader bufferedReader = new BufferedReader(fileReader);
             BufferedReader secondbufferedReader = new BufferedReader(secondFileReader);

             FileWriter fileWriter = new FileWriter("pliki/wyniki.txt");
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

            String lane = bufferedReader.readLine();
            String secondLane = secondbufferedReader.readLine();

            while (lane != null || secondLane != null) {
                if (lane != null) {
                    bufferedWriter.write(lane);
                    bufferedWriter.newLine();
                }
                if (secondLane != null) {
                    bufferedWriter.write(secondLane);
                    bufferedWriter.newLine();
                }
                lane = bufferedReader.readLine();
                secondLane = secondbufferedReader.readLine();
            }

        } catch (IOException e) {
            System.out.println("odjebales!");
        }


    }


}
