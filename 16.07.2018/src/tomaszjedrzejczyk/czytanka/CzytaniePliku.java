package tomaszjedrzejczyk.czytanka;

import java.io.*;
import java.util.Arrays;

public class CzytaniePliku {

    public static void main(String[] args) {

//        try {
//            zadanie142("pliki/plik.txt");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        try {
//            zadanie142a("pliki/plik.txt");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        try {
//            zadanie143("pliki/plik.txt");
//        } catch (IOException e) {
//
//
//        }

        int[] tablica = new int[]{1, 2, 3, 4, 5, 6, 7};
        zadnaie144(tablica);
    }

    static void zadanie142(String scieszkaDoPliku) throws IOException {
        FileReader fileReader = new FileReader(scieszkaDoPliku);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = bufferedReader.readLine();
        while (line != null) {
            System.out.println(line);
            line = bufferedReader.readLine();
        }
    }

    static void zadanie142a(String sciezkaDoPliku) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(sciezkaDoPliku);
        int singleChar = fileInputStream.read();
        while (singleChar != -1) {
            System.out.print((char) singleChar);
            singleChar = fileInputStream.read();


        }
    }

    static void zadanie143(String sicezkaDoPliku) throws IOException {
        FileReader fileReader = new FileReader(sicezkaDoPliku);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String czytanaLinia = bufferedReader.readLine();

        int counterLines = 0;
        int counterChars = 0;

        while (czytanaLinia != null) {
            counterLines++;
            counterChars += czytanaLinia.length();
            czytanaLinia = bufferedReader.readLine();
        }
        System.out.printf("Liczba lini: %s, liczba znakow: %s", counterLines, counterChars);

    }

    static int zadnaie144(int[] tablica) {
        int max = tablica[0];
        boolean czyPokazywacKomunikaty = odczytywacz("pliki/zadanie144.txt");
        for (int liczbaWTabeli : tablica) {
            if (liczbaWTabeli > max) {
                max = liczbaWTabeli;
            }
        }
        if (czyPokazywacKomunikaty){
            System.out.println("przekazana tablica to :");
            System.out.println(Arrays.toString(tablica));
            System.out.println("najwieksza liczba w tablicy to "+max);
        }
        return max;
    }

    static boolean odczytywacz(String sciezkaDoPliku) {
        boolean robienieNaBoolean = false;
        try (FileReader fileReader = new FileReader(sciezkaDoPliku);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {  //dostepne na metodach samo zamykajacych sie autoclosable autozamykanie

            String czytanaLinia = bufferedReader.readLine();
            robienieNaBoolean = Boolean.parseBoolean(czytanaLinia);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return robienieNaBoolean;
    }
}
