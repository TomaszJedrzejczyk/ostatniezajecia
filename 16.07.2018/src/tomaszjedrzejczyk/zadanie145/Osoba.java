package tomaszjedrzejczyk.zadanie145;

public class Osoba {

   private String imie;
   private String nazwisko;
   private int wiek;
   private boolean czyMaDom;

    public Osoba(String imie, String nazwisko, int wiek, boolean czyMaDom) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.czyMaDom = czyMaDom;
    }
}
