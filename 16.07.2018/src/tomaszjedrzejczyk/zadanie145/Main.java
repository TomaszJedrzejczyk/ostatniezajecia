package tomaszjedrzejczyk.zadanie145;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {


        System.out.println(listaOsob("pliki/zadanie144.txt"));


    }

    static List<Osoba> listaOsob(String odczytPliku) throws IOException {
        List<Osoba> listaOsob = new ArrayList<>();

           try( FileReader fileReader = new FileReader(odczytPliku);
            BufferedReader bufferedReader = new BufferedReader(fileReader)){
            String linia = bufferedReader.readLine();

            while (linia != null) {
                String[] splitPoPrzecinku = linia.split(",");
                System.out.println(Arrays.toString(splitPoPrzecinku));
                Osoba osoba = convertArrayToOsoba(splitPoPrzecinku);
                listaOsob.add(osoba);
                linia = bufferedReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listaOsob;
    }

    private static Osoba convertArrayToOsoba(String[] splitPoPrzecinku) {
        return new Osoba(
                splitPoPrzecinku[0],
                splitPoPrzecinku[1],
                Integer.parseInt(splitPoPrzecinku[2]),
                Boolean.parseBoolean(splitPoPrzecinku[3])

        );
    }

    /**
     * 1.odczytaj zawartosc pliku
     * 2.podziel linie po przecinkach co da nam tablice
     * 3.utworz obiekt klacy osoba wypelniajac go wartosciami z tablicy
     * 4.dodaj osoby do listy
     * 5.zwroc liste
     *
     * *ZADANIE #145*
     * Przygotuj plik .csv (gdzie wartości oddzielone są przecinkami - `,`),
     * który będzie zawierał reprezentację obiektów klasy `Osoba`. Metoda ma odczytać plik i zwrócić *listę* obiektów klasy `Osoba`.
     */

    public class Dictionary {

        private Map<String,String> slownik = new HashMap<>();
        public Dictionary(){

        }

        public void newEntry(String key, String value){
            slownik.put(key,value);
        }

        public String look(String key){
            return slownik.getOrDefault(key, "Cant find entry for"+key);
        }
    }

}

