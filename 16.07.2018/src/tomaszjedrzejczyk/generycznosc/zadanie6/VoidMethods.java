package tomaszjedrzejczyk.generycznosc.zadanie6;

import tomaszjedrzejczyk.generycznosc.zadanie1.Apple;
import tomaszjedrzejczyk.generycznosc.zadanie2.UniversalBox;
import tomaszjedrzejczyk.generycznosc.zadanie5.FriutBox;
import tomaszjedrzejczyk.generycznosc.zadanie5.Fruit;

public class VoidMethods {

    public static void method1(FriutBox<Apple> friutBox){
        friutBox.showFruit();
        friutBox.getFruit().sayHello();
    }

    public static void mathod2(FriutBox<?> friutBox){   //akceptuje dowolnego fruit boxa
        friutBox.showFruit();
        friutBox.getFruit().sayHello();
    }

    public static void method3(FriutBox<? extends Apple> friutBox){ //sortuje i wypuszcza z calego boxa tylko Apple
        friutBox.showFruit();
    }

    public static void method4(FriutBox<? super Apple> friutBox){ //super dziala w odwrota strone co extends
       friutBox.showFruit();
    }
}
