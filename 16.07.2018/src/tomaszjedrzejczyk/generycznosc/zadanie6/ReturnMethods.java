package tomaszjedrzejczyk.generycznosc.zadanie6;

import tomaszjedrzejczyk.generycznosc.zadanie1.Apple;
import tomaszjedrzejczyk.generycznosc.zadanie1.Orange;
import tomaszjedrzejczyk.generycznosc.zadanie5.Fruit;

import java.util.Collections;
import java.util.List;

public class ReturnMethods {

    public static <T> T method1(T element) {
        System.out.println(element.toString());
        return element;
    }

    public static <T extends Fruit> T method2(T element) {
        element.sayHello();
        return element;
    }

    public static <A> A method3(A first, A second) {
        System.out.println(first.getClass().getSimpleName());
        System.out.println(second.getClass().getSimpleName());
        return second;
    }

    private static <A extends Apple, O extends Orange> A method4(A apple, O orange) {
        return apple;
    }

    public static int ConvertBinaryArrayToInt(List<Integer> binary) {
        Collections.reverse(binary);
        int wynik = 0;
        for (int i = 0; i < binary.size(); i++) {
            if (binary.get(i) == 1) {
                wynik += Math.pow(2, i);
            }
        }
        return wynik;
    }
}
