package tomaszjedrzejczyk.generycznosc.zadanie5;

import tomaszjedrzejczyk.generycznosc.zadanie1.Orange;

public class FriutBox<T extends Fruit> {
    private T fruit;


    public FriutBox(T fruit) {
        this.fruit = fruit;
    }

    public T getFruit() {
        return fruit;
    }

    public void setFruit(T fruit) {
        this.fruit = fruit;
    }

    public void showFruit(){
        fruit.sayHello();
    }
}
