package tomaszjedrzejczyk.generycznosc.zadanie2;

public class UniversalBox {
   private Object element;

    public UniversalBox(Object element) {
        this.element = element;
    }

    public Object getElement() {
        return element;
    }

    public void setElement(Object element) {
        this.element = element;
    }
}
