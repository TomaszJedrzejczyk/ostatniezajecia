package tomaszjedrzejczyk.generycznosc;

import tomaszjedrzejczyk.generycznosc.zadanie1.Apple;
import tomaszjedrzejczyk.generycznosc.zadanie1.AppleBox;
import tomaszjedrzejczyk.generycznosc.zadanie1.Orange;
import tomaszjedrzejczyk.generycznosc.zadanie1.OrangeBox;
import tomaszjedrzejczyk.generycznosc.zadanie2.UniversalBox;
import tomaszjedrzejczyk.generycznosc.zadanie3.GenericBox;
import tomaszjedrzejczyk.generycznosc.zadanie4.BigUniversaBox;
import tomaszjedrzejczyk.generycznosc.zadanie5.FriutBox;
import tomaszjedrzejczyk.generycznosc.zadanie6.ReturnMethods;
import tomaszjedrzejczyk.generycznosc.zadanie6.VoidMethods;

public class Main {
    public static void main(String[] args) {

//        zadanie1();
//        zadanie2();
//        zadanie3();
//        zadanie4();
//        zadanie5();
//        zadanie6();
        zadanie7();
        zadanie8();
        zadanie9();
    }

    private static void zadanie9() {
        ReturnMethods.method3("hajsks",1);
    }

    private static void zadanie8() {
        ReturnMethods.method2(new Orange());
        ReturnMethods.method2(new Apple());
//        ReturnMethods.method2("string");
    }

    private static void zadanie7() {
        ReturnMethods.method1("moj napis");
        ReturnMethods.method1(true);
        ReturnMethods.method1(12);
    }

    private static void zadanie6() {
        FriutBox<Apple> friutBox = new FriutBox<>(new Apple());
        VoidMethods.method1(friutBox);
//        VoidMethods.method1(friutBox1);//typ elementow w boxie ogranicz parametry metody
        FriutBox<Orange> friutBox1 = new FriutBox<>(new Orange());
        VoidMethods.mathod2(friutBox1);
        VoidMethods.mathod2(friutBox);
    }

    private static void zadanie1() {
        Apple apple = new Apple();
        AppleBox appleBox = new AppleBox(apple);

        Orange orange = new Orange();
        OrangeBox orangeBox = new OrangeBox(orange);


    }

    private static void zadanie2() {
        UniversalBox universalBox = new UniversalBox(new Apple());
        System.out.println(universalBox.getElement().getClass().getSimpleName());

        universalBox.setElement(new Orange());
        System.out.println(universalBox.getElement().getClass().getSimpleName());

        universalBox.setElement(true);
        System.out.println(universalBox.getElement().getClass().getSimpleName());

        universalBox.setElement("string");
        System.out.println(universalBox.getElement().getClass().getSimpleName());
    }

    private static void zadanie3() {
        Apple apple = new Apple();
        GenericBox<Apple> genericBox = new GenericBox<>(apple);
        System.out.println(genericBox.getElement().getClass().getSimpleName());
//        genericBox.setElement(new Orange()); //nie jestemsy w stanie podac innego typu niz przy deklaracji

        GenericBox<String> genericBox1 = new GenericBox<>("String jakis");
        System.out.println(genericBox1.getElement());
        genericBox1.setElement("nowy string jakis");
        System.out.println(genericBox1.getElement());
    }

    private static void zadanie4() {
        BigUniversaBox<Apple, Orange> bigUniversaBox = new BigUniversaBox<>(new Apple(), new Orange());
        System.out.printf("Pierwszy: %s , Drugi: %s \n",
                bigUniversaBox.getFirst().getClass().getSimpleName(),
                bigUniversaBox.getSecond().getClass().getSimpleName());

        BigUniversaBox<String, String> bigUniversaBox1 = new BigUniversaBox<>("Pierwszy napis", "Drugi napis");
        System.out.println(bigUniversaBox1.getFirst());
        System.out.println(bigUniversaBox1.getSecond());
    }

    private static void zadanie5() {
        FriutBox<Apple> friutBox = new FriutBox<>(new Apple());
        friutBox.showFruit();

        FriutBox<Orange> friutBox1 = new FriutBox<>(new Orange());
        friutBox1.showFruit();
    }


}
