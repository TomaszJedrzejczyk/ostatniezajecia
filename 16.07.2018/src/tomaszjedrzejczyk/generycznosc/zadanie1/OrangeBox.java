package tomaszjedrzejczyk.generycznosc.zadanie1;

public class OrangeBox {

    private Orange orange;

    public OrangeBox(Orange orange) {
        this.orange = orange;
    }

    public Orange getApple() {
        return orange;
    }
}
