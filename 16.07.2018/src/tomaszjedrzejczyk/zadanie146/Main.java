package tomaszjedrzejczyk.zadanie146;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {


    //Utwórz metodę która odczytuje zawartości ze Scannera (do momentu podania pustej linii)
    // i zapisuje wartości do pliku

    public static void main(String[] args) {
        try {
            zadanie146("pliki/zadanie146.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    static void zadanie146(String plikDoZapisu) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Napisz cos: ");
        FileWriter fileWriter = new FileWriter(plikDoZapisu, true); //append:true - dodaje zawartos do pliku, nadpisuje
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        while (true) {
            String napis = scanner.nextLine();
            if (napis.equals("")) {
                break;
            }
            bufferedWriter.write(napis);
            bufferedWriter.newLine(); // lamie linie w pliku
        }
        bufferedWriter.close();

    }
}
