package tomaszjedrzejczyk.wielowatkowosc;

public class MyThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i <= 10; i++) {
            System.out.printf("%s pobieranie danych %s%%\n",
                    Thread.currentThread().getId(),i*10 );

            try {
                Thread.sleep(500); // 500 to jest pol sekundy
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
