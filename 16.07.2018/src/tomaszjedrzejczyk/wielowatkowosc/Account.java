package tomaszjedrzejczyk.wielowatkowosc;

public class Account {

    private int balance;

    public Account(int balance) {
        this.balance = balance;
    }

    synchronized void wlasc(int wplata){
        balance+= wplata;
    }

    synchronized void wyplac(int wyplata){
        if (balance<wyplata){
            throw new IllegalArgumentException("brak kasy");
        }
        balance-=wyplata;
    }

    public int getBalance() {
        return balance;
    }
}
