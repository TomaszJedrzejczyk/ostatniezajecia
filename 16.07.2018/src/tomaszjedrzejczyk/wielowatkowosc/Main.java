package tomaszjedrzejczyk.wielowatkowosc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
//        method4();
//        method5();
        method6();


    }

    private static void method6() {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        System.out.println("main id: "+Thread.currentThread().getId());
        for (int i = 0; i < 10; i++) {
            Thread zadanieDoWykonania = new Thread() {
                @Override
                public void run() {
                    try {
                        System.out.println("ID watku: " + Thread.currentThread().getId());
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            executorService.submit(zadanieDoWykonania);
        }
        executorService.shutdown();

    }

    private static void method5() {
        final int ILOSC_OPERACJI = 1000;
        final int KWOTA_OPERACJI = 10;
        Account account = new Account(100_000);
        Thread wplacacz = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < ILOSC_OPERACJI; i++) {
                    account.wlasc(KWOTA_OPERACJI);
                }
            }
        };
        Thread wyplacacz = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < ILOSC_OPERACJI; i++) {
                    account.wyplac(KWOTA_OPERACJI);
                }
            }
        };

        System.out.println("poczatkowy stan konta: " + account.getBalance());
        wplacacz.start();
        wyplacacz.start();

        try {
            wplacacz.join();
            wyplacacz.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("koncowy stan konta: " + account.getBalance());
    }

    private static void method4() {
        System.out.println("main id: " + Thread.currentThread().getId());

        MyThread myThread1 = new MyThread();
        MyThread myThread2 = new MyThread();

        myThread1.start();
        myThread2.start();
        try {
            myThread1.join();// watek w ktorym wywolana jest metoda join musi poczekac na koniec pracy watku na ktorym jest wywolany
            myThread2.join(); //laczy watki aby wywolac je na koncu dzialania programu
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("main dziala w wantku: " + Thread.currentThread().getId());

    }

    private static void method3() {
        MyThread myThread1 = new MyThread();
        MyThread myThread2 = new MyThread();

        myThread1.start();
        myThread2.start();
    }

    private static void method2() {
        MyThread myThread = new MyThread();
        myThread.start();
    }

    private static void method1() {
        System.out.println(Thread.currentThread().getId() + " identyfikator maina");
        Thread thread = new Thread() {
            @Override
            public void run() {
                System.out.println("jakis system out");
                System.out.println(Thread.currentThread().getId());
            }
        };
        thread.start();
    }
}
