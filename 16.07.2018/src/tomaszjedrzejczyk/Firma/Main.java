package tomaszjedrzejczyk.Firma;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Pracownik osoba = new Pracownik("Danutka", 35);
        Pracownik osoba2 = new Pracownik("Bozenka", 45);
        Pracownik osoba3 = new Pracownik("Wieslaw", 52);
        Pracownik osoba4 = new Pracownik("Bogdan", 62);

        List<Pracownik> listaOsob = new ArrayList<>(
                Arrays.asList(osoba, osoba2, osoba3, osoba4
                ));
        list(listaOsob);
        System.out.println();

        Collections.sort(listaOsob);
        list(listaOsob);
    }

    static void list(List<Pracownik> lista) {
        for (Pracownik pracownik : lista) {
            System.out.println(pracownik);
        }
    }
}
