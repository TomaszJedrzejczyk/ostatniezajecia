package tomaszjedrzejczyk.Firma;

public class Pracownik implements Comparable<Pracownik> {

    private String imie;
    private int wiek;

    public Pracownik(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    @Override
    public int compareTo(Pracownik o) {
        if (wiek == o.wiek) {
            return 0; //obiekty sa takie same
        } else if (wiek < o.wiek) {
            return -1; //obecny jest pierwszy
        } else {
            return 3; //przekazywany obiekt jest pierwszy
        }
    }

    @Override
    public String toString() {
        return String.format("%s (%s) ", imie, wiek);
    }
}
