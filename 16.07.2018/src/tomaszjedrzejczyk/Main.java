package tomaszjedrzejczyk;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws WlasnyWyjatek {

//        System.out.println(zadanie128(25));
//        try {                                                       // staramy sie zlapac wyjatek
//            System.out.println(zadanie128(-36));
//        } catch (RuntimeException wyjatekLiczbaujemna) {             // deklarujemy jaki wyjatek ma byc zlapany
//            wyjatekLiczbaujemna.printStackTrace();                  //pdajesz to co napisales w throw
//            System.out.println(wyjatekLiczbaujemna.getMessage());   //podaje to co mielismy wthrow jako normaln println
//            System.out.println("Wyniki to liczba ujemna!");         // dajemy komentarz dla uzytkonika z tym co poszlo nie tak
//        }
//        System.out.println(zadanie128(36));
//
        int[] tablica = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
//
//        zadanie129(tablica);

//        try {
//            System.out.println(zadanie130());
//        } catch (WlasnyWyjatek wlasnyWyjatek) {
//            wlasnyWyjatek.wlasnaTresc();
//        }

//        Scanner scanner = new Scanner(System.in);
//        System.out.print("Podaj liczbe: ");
//        int liczba = scanner.nextInt();
//        String napis = "Przykladowy napis";
//
//        try {
//            zadanie131(napis, liczba);
//        } catch (WlasnyWyjatek wlasnyWyjatek) {
//            wlasnyWyjatek.printStackTrace();
//        }


            Scanner scanner =new Scanner(System.in);
            System.out.print("podaj pierwsza liczbe: ");
            int pierwszaLiczba = scanner.nextInt();
            System.out.print("podaj druga liczbe: ");
            int drugaLiczba = scanner.nextInt();
            System.out.println(zadanie135(pierwszaLiczba,drugaLiczba));





    }

    static double zadanie135(int pierwszaLiczba, int drugaLiczba){
        return (double) pierwszaLiczba/drugaLiczba; //opis w nawiasach to zutowanie czyli podmiana ostatecznej wartosci na inny typ
    }

    /**
     * Utwórz metodę która przyjmuje jeden parametr (typu `int`) a następnie liczy pierwiastek z podanej liczby (użyj `Math.sqrt()`).
     * Jeśli podany parametr będzie liczbą ujemną rzuć wyjątek (np. `IllegalArgumentException`) z komunikatem błędu. (edited)
     */

    static double zadanie128(double liczba) {
        if (liczba < 0) {
            throw new RuntimeException("Podania liczba jest mniejsza od zera!");    //podajemy wyjatek jezeli liczba bedzie ujemna
        }
        return Math.sqrt(liczba);
    }

    /**
     * Utwórz metodę która przyjmuje tablicę, a następnie przy wykorzystaniu `Scannera` użytkownik musi podać indeks parametru który ma zostać wyświetlony.
     * W przypadku indeksu poza zakresem (tzn. gdy wystąpi wyjątek) zapytaj użytkownika o indeks ponownie (do skutku).
     */

    static void zadanie129(int[] tablica) {
        Scanner scanner = new Scanner(System.in);
        for (; ; ) {
            System.out.print("Podaj indeks parametru: ");
            try {
                int wybranaPozycja = scanner.nextInt();
                System.out.println(tablica[wybranaPozycja]);
                break;
            } catch (InputMismatchException e) {
                System.out.println("Błędne dane wejsciowe! Podaj cyfre.");
                scanner.nextLine();
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Przekroczyles zakres tablicy! zakres jest od 0 do " + (tablica.length - 1));
            }
        }
    }

    static int zadanie130(int... przekazywaneLiczby) throws WlasnyWyjatek {
        if (przekazywaneLiczby == null || przekazywaneLiczby.length == 0) {
            throw new WlasnyWyjatek("Wystapil wlasny wyjatek");
        }
        int suma = 0;
        for (int liczba : przekazywaneLiczby) {
            suma += liczba;
        }
        return suma;
    }

    /**
     * Utwórz metodę, która przyjmuje dwa parametry - napis (`String`) oraz liczbę (`int`) wcześniej odczytaną `Scanner`-em od użytkownika.
     * Metoda ma wyświetlić podany napis, przekazaną liczbę razy.
     * W przypadku gdy liczba wystąpień będzie mniejsza bądź równa zero powinien zostać rzucony własny wyjątek (dziedziczący po `RuntimeException`).
     */

    static void zadanie131(String napis, int liczba) throws WlasnyWyjatek {

        if (liczba <= 0) {
            WlasnyWyjatek wlasny = new WlasnyWyjatek("liczba wystapien jest mniejsza lub rowna 0");
            throw wlasny;
        }

        for (int i = 0; i < liczba; i++) {
            System.out.println(napis);
        }
    }

    /**
     * Utwórz metodę która przyjmuje niepustą liste lub tablicę a następnie w nieskończonej pętli je wyświetla (zaczynając od pozycji `0`).
     * Pętla ma zostać przerwana po wyjściu poza zakres listy (tzn. po przekroczeniu jej rozmiaru).
     */

    static void zadanie132(int[] tablica) {

        for (int wartosc = 0; ; wartosc++) {
            try {
                System.out.println(tablica[wartosc]);
            } catch (IndexOutOfBoundsException e) {
                System.out.println("<-- przekrocznie zakrezu tablicay");
                break;
            }
        }
    }

    static void zadanie132a(int[] tablica) {

        try {
            for (int wartosc = 0; ; wartosc++) {
                System.out.println(tablica[wartosc]);
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("<-- przekrocznie zakrezu tablicay");
        }
    }

    static int zadanie133(int dzielna, int dzielnik) throws WlasnyWyjatek {
        int wynik = 0;
        if (dzielnik==0){
            wynik=0;
            throw  new WlasnyWyjatek("podzieliles przez 0!");
        }
        return wynik = dzielna / dzielnik;
    }

    static void zadanie134(int liczba){
        Scanner scanner = new Scanner(System.in);

        for (;;){
            System.out.println("podaj liczbe: ");
            try{
                liczba = scanner.nextInt();
                System.out.println(liczba);
            }catch (InputMismatchException e){
                System.out.println("nie podales liczby!");
                scanner.nextLine();
            }
        }
    }

    static void zadanie134a(int liczba) throws InputMismatchException{
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("podaj liczbe ");
            if (!scanner.hasNextInt()){
                throw  new InputMismatchException("nie podales liczby!");
            }
        }
    }

}
