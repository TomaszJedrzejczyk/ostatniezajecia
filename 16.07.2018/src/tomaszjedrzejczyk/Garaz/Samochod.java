package tomaszjedrzejczyk.Garaz;

import java.util.Objects;

public class Samochod {
    private String nrRejestracji;
    private Kolor kolor;


    public Samochod(String nrRejestracji, Kolor kolor) {
        this.nrRejestracji = nrRejestracji;
        this.kolor = kolor;
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) {
//            return false;
//        }
//        if (obj instanceof Samochod) {
//            Samochod s = (Samochod) obj;
//            return nrRejestracji.equals(s.nrRejestracji)
//                    &&
//                    kolor == s.kolor;
//        }
//        return false;
//    }


    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Samochod samochod = (Samochod) o;
        return Objects.equals(nrRejestracji, samochod.nrRejestracji) &&
                kolor == samochod.kolor;
    }

    @Override
    public int hashCode() {
        return nrRejestracji.hashCode()*31
                +
                kolor.hashCode()*31;
    }

    enum Kolor {
        CZERWONY,
        NIEBIESKI,
        CZARNY,
        BIALY,
    }
}
