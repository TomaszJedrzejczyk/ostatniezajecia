package tomaszjedrzejczyk.calendar;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.DuplicateFormatFlagsException;

public class Main {
    public static void main(String[] args) {
        /** CZAS */
//        metho1("18:35"); //format rodzielajacy ":" musi byc taki sam tu jak i w metodzie
//        method2();
//        metho3("12:45:32");

        /** DATA */
//        method4();
//        method5("20.03.2018");
//        method6("21.05.1998");
//        method7("10-12-1999");

        /** DATA */
//        method8();
//        method9("27-12-2001 18:33");
//        method10("10:03", "12:50");
//        method11("20.04.2014", "15.06.2018");
        method12("07.07.1989 15:45", "23.07.2018 12:15");

    }

    private static void method12(String dateOne, String dateTwo) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

        LocalDateTime date1 = LocalDateTime.parse(dateOne, formatter);
        LocalDateTime date2 = LocalDateTime.parse(dateTwo, formatter);

        long years = ChronoUnit.YEARS.between(date1, date2);
        long months = ChronoUnit.MONTHS.between(date1, date2);
        long weeks = ChronoUnit.WEEKS.between(date1, date2);
        long days = ChronoUnit.DAYS.between(date1, date2);
        long hours = ChronoUnit.HOURS.between(date1, date2);
        long minutes = ChronoUnit.MINUTES.between(date1, date2);
        long seconds = ChronoUnit.SECONDS.between(date1, date2);
        long milis = ChronoUnit.MILLIS.between(date1, date2);
        long nanos = ChronoUnit.NANOS.between(date1, date2);

        System.out.printf("pomiedzy %s a %s roznica wynosi: ", dateOne, dateTwo);
        System.out.println(years + " lat");
        System.out.println(months + " miesiecy");
        System.out.println(weeks + " tygodni");
        System.out.println(days + " dni");
        System.out.println(hours + " godzin");
        System.out.println(minutes + " minut");
        System.out.println(seconds + " sekund");
        System.out.println(milis + " milisekund");
        System.out.println(nanos + " nanosekund");
    }

    private static void method11(String dateOne, String dateTwo) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        LocalDate date1 = LocalDate.parse(dateOne, formatter);
        LocalDate date2 = LocalDate.parse(dateTwo, formatter);

        Period period = Period.between(date1, date2); // do sprawdzania ile dni roznicy jest pomiedzy tymi dniami

        System.out.println(period.getDays()); //prawdza ile dni jest pomiedzy tymi datami, ale niestety jednie ilosc dni, nie porownuje calych okresow

    }

    private static void method10(String timeOne, String timeTwo) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime localTime1 = LocalTime.parse(timeOne, formatter);
        LocalTime localTime2 = LocalTime.parse(timeTwo, formatter);

        Duration duration = Duration.between(localTime1, localTime2); //porownujemy czsy pomiedzy

        System.out.println(duration.getSeconds()); // sprawdzamy ile sekund bylo pomiedzy 2 podanymi godzinami
    }


    private static void method9(String myDateTima) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

        LocalDateTime dateTime = LocalDateTime.parse(myDateTima, formatter); // zparsuj moje wprowadzane dane z formaterem

        DateTimeFormatter putputDateTime = DateTimeFormatter.ofPattern("HH:mm ; dd MMMM yy");

        System.out.println(dateTime.format(putputDateTime));

    }

    private static void method8() {
        LocalDateTime localDateTime = LocalDateTime.of(1994, 1, 28, 12, 45, 55);

        DayOfWeek dayOfWeek = localDateTime.getDayOfWeek(); //wyciaga ktory to dzien tygodnia
        int dayOfMonth = localDateTime.getDayOfMonth(); //wyciaga dzien miesiaca
        Month month = localDateTime.getMonth(); //wyciaga miesiac
        long minuteOfDay = localDateTime.get(ChronoField.MINUTE_OF_DAY); //wyciaga ktora to jst minuta z podanego dnia
        System.out.println(minuteOfDay);
        System.out.println(month);
        System.out.println(dayOfMonth);
        System.out.println(dayOfWeek);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy ; HH:mm");
        System.out.println(localDateTime.format(formatter));
    }

    private static void method7(String myDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        LocalDate date = LocalDate.parse(myDate, formatter);

        System.out.println(date.format(DateTimeFormatter.ISO_LOCAL_DATE)); // wybralismy format z gotowego Date time formtera; oficjalne ISO
    }

    private static void method6(String myDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        LocalDate date = LocalDate.parse(myDate, formatter);

        DateTimeFormatter outputFormater = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        System.out.println("Pierwsza data to: " + myDate + " ; nowa data to: " + date.format(outputFormater));
        System.out.println(date.format(DateTimeFormatter.ofPattern("dd MM yy")));  //21 05 89 od dlugosci zalezy rodza wyzwietlenia
        System.out.println(date.format(DateTimeFormatter.ofPattern("dd MMM yy"))); //21 maj 89
        System.out.println(date.format(DateTimeFormatter.ofPattern("dd MMMM yyyy"))); // 21 maja 1998
    }

    private static void method5(String myDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        LocalDate date = LocalDate.parse(myDate, formatter);

        System.out.println(myDate + " to " + date.get(ChronoField.DAY_OF_YEAR) + " dzien roku");

    }

    private static void method4() {
        LocalDate date = LocalDate.of(2018, Month.JULY, 7);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        System.out.print(date.format(formatter));
        System.out.println(formatter.format(date));
    }

    private static void metho3(String myTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss"); // opisujemy w jakiej formie jest data

        LocalTime localTime = LocalTime.parse(myTime, formatter); //tworzymy nowy obiekt czytany przez java

        DateTimeFormatter outputFormater = DateTimeFormatter.ofPattern("HH mm SS"); //okreslamy nowy sposob zapisywania godziny

        String newTime = localTime.format(outputFormater); // tworzymy nowego stirnga do ktorego przypisujemy localTime i formatujemy go przy uzyciu Datatime formater na nowy format przez nas zrobiony

        System.out.println(newTime);
    }


    private static void method2() {
        LocalTime time = LocalTime.of(17, 18, 45);

        System.out.println(time.getSecond());
    }

    private static void metho1(String time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm"); //ofpattern przygotowuje ze dane wejsciowe beda w tym formacie

        LocalTime localTime = LocalTime.parse(time, formatter); //localTime jest juz javova reprezentacja mozna na niej dzialac

        int hour = localTime.get(ChronoField.HOUR_OF_DAY); //wyciagnelismy ktora to jest godzina dnia
        int minute = localTime.get(ChronoField.MINUTE_OF_DAY); // wyciagamy minute dnia , jako parametr pola ChronoField trzeba wywolac enuma

        System.out.printf("Mamy %s minut po godzinie %s-tej", minute, hour);
    }
}
