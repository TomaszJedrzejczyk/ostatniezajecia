package tomaszjedrzejczyk.zadanie149;

import tomaszjedrzejczyk.zadanie145.Osoba;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        List<User> userList = zadanie149("pliki/logs.txt");

        for (int i = 3; i > 0; i--) {
            User logedUser = loginFromUser();
            if (contain(userList, logedUser)){
                System.out.println("Zalogowales sie!");
                System.out.println(userList);
                break;
            }else{
                System.out.println("Zły login lub hasło... Pozstało Ci "+(i-1)+" Zaloguj się ponownie! ");
            }
        }

    }

    //Utwórz metodę która pobiera od użytkownika login i hasło, a następnie sprawdza czy podane dane są prawidłowe
    // (*weryfikując to z plikiem*, w którym wartości rozdzielone są spacją, a każdy z użytkowników jest w nowej linii)

    static List<User> zadanie149(String filePath) throws IOException {
        FileReader fileReader = new FileReader(filePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String lane = bufferedReader.readLine();
        List<User> userList = new ArrayList<>();
        while (lane != null) {
            String[] splitBySpace = lane.split(" ");
            User user = convertArrayToUser(splitBySpace);
            userList.add(user);
            lane = bufferedReader.readLine();
            /**
             * 1.czytanie pojedynczej lini
             * 2.podzieleni lini po spacji co daje na tablice
             * 3.utworzenie obiektu klasy user wykorzystujac podzielona linie
             * 4.dodanie obiektu to listy
             * 5. sprawdzenie czy lista zawiera uzytkownieka pobranego z konsoli
             */
        }
        return userList;
    }

    static boolean contain(List<User> list, User userThatIsLogin) {
        for (User user : list) {
            if (user.equals(userThatIsLogin)) {
                return true;
            }
        }
        return false;
    }

    private static User convertArrayToUser(String[] split) {
        return new User(
                split[0],
                split[1]);
    }

    static class User {
        private String login;
        private String password;

        public User(String login, String password) {
            this.login = login;
            this.password = password;
        }

        @Override
        public boolean equals(Object obj) {
            User user1 = (User) obj;
            return user1.password.equals(this.password)
                    && user1.login.equals(this.login);

        }

        @Override
        public String toString() {
            return "login: " + login + "/ haslo: " + password;
        }
    }

    static User loginFromUser() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj login: ");
        String login = scanner.nextLine();
        System.out.print("Podaj hasło: ");
        String password = scanner.nextLine();

        return new User(login, password);
    }
}
