package grupaSDA;

public class Rownanie {

    int a;
    int b;
    int c;

    public Rownanie() {
    }

    public Rownanie(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    int wynik() {

        return (int) (Math.pow(a, 2) + Math.pow(b, 3) + Math.pow(c, 4));
    }

    boolean porownanie(int liczba) {
        return wynik() > liczba;
    }


}
