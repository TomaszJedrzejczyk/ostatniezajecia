package grupaSDA;

public class ZadanieZCodeWars {

    static boolean check (String sentence){
        int dlugosc = sentence.length();
        sentence = sentence.toLowerCase();
        for (char litera = 'a'; litera<= 'z'; litera++) {
           sentence = sentence.replace(String.valueOf(litera),"");
           if (sentence.length()<dlugosc){
               dlugosc =sentence.length();
           }else {
               return false;
           }
        }
        return true;
    }
}
