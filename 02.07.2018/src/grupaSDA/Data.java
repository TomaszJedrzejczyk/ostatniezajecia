package grupaSDA;

public class Data {

    int dzien;
    int miesiac;
    int rok;

    public Data() {
    }

    public Data(int dzien, int miesiac, int rok) {
        this.dzien = dzien;
        this.miesiac = miesiac;
        this.rok = rok;
    }

    boolean ustawienieDnia() {
        return dzien > 31 || dzien < 0 ? true : false;
    }

    String wyswietlDate() {
        return wyswietlDate("-");
    }

    String wyswietlDate(String separator) {
        return String.format("%s %s %s %s %s", dzien, separator, miesiac, separator, rok);
    }

    boolean zaktualizujDzien(int nowydzien) {
        if (nowydzien >= 0 && nowydzien <= 31) {
            dzien = nowydzien;
            return true;
        }
        return false;
    }
}
