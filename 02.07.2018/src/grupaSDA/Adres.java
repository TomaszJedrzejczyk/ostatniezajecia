package grupaSDA;

public class Adres {

    String ulica;
    int numerDomu;
    int numerMieszkania;
    int kodPocztowy;
    String miasto;

    public Adres() {
    }

    public Adres(String ulica, int numerDomu, int numerMieszkania) {
        this.ulica = ulica;
        this.numerDomu = numerDomu;
        this.numerMieszkania = numerMieszkania;
    }

    public Adres(String ulica, String miasto, int numerDomu, int numerMieszkania, int kodPocztowy) {
        this.ulica = ulica;
        this.miasto = miasto;
        this.numerDomu = numerDomu;
        this.numerMieszkania = numerMieszkania;
        this.kodPocztowy = kodPocztowy;
    }

    String pelenAdres (){
        return String.format("Hej mieszkam na %s w %s , %s w bloku %s pod numerem %s .", ulica, miasto, kodPocztowy, numerDomu, numerMieszkania);
    }

    String ulicaWrazZNumerami (){
        return String.format("%s numer %s mieszkania %s",ulica,numerDomu,numerMieszkania);
    }

}
