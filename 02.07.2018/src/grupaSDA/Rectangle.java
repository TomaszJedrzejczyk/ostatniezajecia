package grupaSDA;

public class Rectangle {

    int dluzszyBok;
    int krotszyBok;

    public Rectangle(int dluzszyBok, int krotszyBok) {
        this.dluzszyBok = dluzszyBok;
        this.krotszyBok = krotszyBok;
    }

    int pole () {
        return  dluzszyBok * krotszyBok;
    }

    int powieszchnia () {
       return (2*krotszyBok)+(2*dluzszyBok);

    }

    static boolean porownanie(Rectangle p1, Rectangle p2) {
        return p1.pole()==p2.pole();
    }
}
