package mojProject;

public class Osoba {
    private String imie;
    private int identyfitator;

    public Osoba(String imie, int identyfitator) {
        this.imie = imie;
        this.identyfitator = identyfitator;
    }

    @Override
    public String toString() {
        return String.format("mam na imie %s , ,pj identyfikator to %s",imie,identyfitator);
    }

    public Integer getId() {
        return identyfitator;
    }
}
