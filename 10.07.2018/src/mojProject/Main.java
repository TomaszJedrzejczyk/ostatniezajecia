package mojProject;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        Map<Integer, Osoba> testowa = new HashMap<>();
        Osoba person1 = new Osoba("bobek", 1973793);
        Osoba person2 = new Osoba("korek", 1930193);

        testowa.put(person1.getId(),person1);
        testowa.put(person1.getId(),person2);

        System.out.println(testowa);



//        System.out.println(zadanie122());
//        zadanie123(zadanie122());
//
//        Map<String, Integer> nowaMapa = new HashMap<>();
//        nowaMapa.put("dupa", 1);
//        nowaMapa.put("dupa2", 2);
//        nowaMapa.put("dupa3", 7);
//        nowaMapa.put("dupa4", 6);

//        System.out.println(zadanie124(nowaMapa));

        Map<String, List<Integer>> map = zadanie125("MAPA SATANISTOW");
        System.out.println(zadanie126(map));

    }


    static List<Integer> zadanie115(List<Integer> lista, int podzielacz) {
        List<Integer> zwracanaLista = new ArrayList<>();
        for (int i = podzielacz; i < lista.size(); i++) {
            zwracanaLista.add(0, lista.get(i));
        }
        return zwracanaLista;
    }

    static List<Integer> zadanie115a(List<Integer> list, int podzielaczka) {
        List<Integer> zwracanaLista = list.subList(podzielaczka, list.size());
        Collections.reverse(zwracanaLista);
        return zwracanaLista;
    }

    static int zadanie116(List<Integer> lista, int poczatek, int koniec) {
        int max = lista.get(poczatek);
        int min = lista.get(poczatek);
        for (int i = poczatek; i < koniec; i++) {
            int obecnaLiczba = lista.get(i);
            if (obecnaLiczba > max) {
                max = obecnaLiczba;
            }
            if (obecnaLiczba < min) {
                min = obecnaLiczba;
            }
        }
        int wynik = Math.abs(max - min);
        if (wynik >= lista.size()) {
            return -1;
        }
        return lista.get(wynik);

    }

    /**
     * Utwórz metodę, która przyjmuje dwa parametry - listę liczb (np. `ArrayList`) oraz poszukiwana liczba.
     * Metoda ma za zadanie zwrócić informację ile razy wystąpiła liczba przekazana jako drugi parametr.
     */
    static int zadanie117(List<Integer> lista, int poszukiwanaLiczba) {
        int licznik = 0;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) == poszukiwanaLiczba) {
                licznik++;
            }
        }
        return licznik;
    }

    static Set<Integer> zadanie118(List<Integer> lista) {
        return new HashSet<Integer>(lista);

    }

    /**
     * Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania duplikatu a następnie zwraca set (np. `HashSet`) (z podanymi liczbami)
     */

    static Set<Integer> zadanie119() {
        Scanner scanner = new Scanner(System.in);
        Set<Integer> tablica = new HashSet<>();
        for (int i = 1; ; i++) {
            System.out.print("podaj liczby [" + i + "]: ");
            int konsola = scanner.nextInt();
            if (!tablica.add(konsola)) {
                break;
            }

        }
        return tablica;
    }

    /**
     * Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania wartości `-1`
     * a następnie zwraca *MAPĘ* wartości,
     * gdzie kluczem jest liczba a wartością jest liczba jej wystąpień.
     */

    static Map<Integer, Integer> mapa() {
        Scanner scanner = new Scanner(System.in);
        Map<Integer, Integer> map = new HashMap<>();

        while (true) {
            System.out.print("podaj liczbe: ");
            int konsola = scanner.nextInt();
            if (konsola == -1) {
                break;
            }
            if (map.containsKey(konsola)) {
                int iloscWystapien = map.get(konsola);
                map.put(konsola, iloscWystapien + 1);
            } else {
                map.put(konsola, 1);
            }
        }
        return map;
    }

    /**
     * Utwórz metodę, która przyjmuje trzy parametry. Pierwszy - liczba liczb do wylosowania,
     * druga to początek zakresu losowania, trzecia to koniec zakresu losowania.
     * Metoda ma zwrócić mapę gdzie kluczem jest liczba a wartością jest liczba jej wystąpień.
     */

    static Map<Integer, Integer> zadanie121(int liczbaLiczbDoWyswietlania, int poczatekZakresu, int koniecZakresu) {
        Map<Integer, Integer> map = new HashMap<>();
        Random random = new Random();
        int przesuniecie = koniecZakresu - poczatekZakresu;

        for (int i = 0; i < liczbaLiczbDoWyswietlania; i++) {
            int wylosowanaWartosc = random.nextInt(przesuniecie + 1) + poczatekZakresu;
            if (map.containsKey(wylosowanaWartosc)) {
                map.put(wylosowanaWartosc, map.get(wylosowanaWartosc) + 1);
            } else {
                map.put(wylosowanaWartosc, 1);
            }
        }
        return map;
    }

    public static String numericals(String s) {
        Map<String, Integer> map = new HashMap<>();
        StringBuilder cosTam = new StringBuilder();
        String[] array = s.split("");
        for (String letter : array) {
            int nowaWartosc = map.getOrDefault(letter, 0) + 1;
            map.put(letter, nowaWartosc);
            cosTam.append(nowaWartosc);
        }

        return cosTam.toString();
    }

    /**
     * Utwórz metodę, które zwróci mapę (z conajmniej 10 kluczami) gdzie kluczem jest skrót kraju (np. “PL”, “DE”, “ES”),
     * a wartościami pełna nazwa (np. `Polska`, `Niemcy`, `Hiszpania`)
     */

    static Map<String, String> zadanie122() {
        Map<String, String> mapa = new HashMap<>();
        mapa.put("PL", "polska");
        mapa.put("DE", "niemcy");
        mapa.put("ES", "hiszpania");
        mapa.put("SE", "san escobar");

        return mapa;
    }

    /**
     * Utwórz metodę, która przyjmuje jeden parametr - mapę w postaci `String : String`.
     * Metoda ma wyświetlić zawartość przekazanej mapy (w postaci `"PL" -> "Polska"`)
     */

    static void zadanie123(Map<String, String> map) {
        for (Map.Entry<String, String> pozycja : map.entrySet()) {  //eater - skrot do foreach
            System.out.printf("%s --> %s\n", pozycja.getKey(), pozycja.getValue());
        }
    }

    /**
     * Utwórz metodę, która przyjmuje mapę (w postaci `String : Integer`).
     * Metoda ma zwrócić `String` złożony z kluczy (oddzielonych spacjami) dla których wartość jest powyżej `5`.
     */

    static String zadanie124(Map<String, Integer> map) {
        StringBuilder nazwa = new StringBuilder();
        for (Map.Entry<String, Integer> pozycja : map.entrySet()) {
            if (pozycja.getValue() > 5) {
                nazwa.append(pozycja.getKey());
                nazwa.append(" ");
            }
        }
        return nazwa.toString();

    }

    /**
     * Utwórz metodę, która przyjmuje jeden parametr - napis (typu `String`). Metoda ma zwrócić mapę w postaci `String : List<Integer>`,
     * w której jako klucze mają być litery (z napisu wejściowego) a jako wartości - *pozycje* na jakich występują one w napisie wejściowym.
     */

    static Map<String, List<Integer>> zadanie125(String napis) {
        String[] array = napis.split("");   //wywoluje metode split ktora dzieli String po kazdym znaku
        Map<String, List<Integer>> nowaMapa = new HashMap<>();
        for (int i = 0; i < array.length; i++) {
            List<Integer> lista;
            if (nowaMapa.containsKey(array[i])) {
                lista = nowaMapa.get(array[i]);
            } else {
                lista = new ArrayList<>();
            }
            lista.add(i);
            nowaMapa.put(array[i], lista);
        }
        return nowaMapa;
    }

    /**
     * Utwórz metodę, która przyjmuje jeden parametr - mapę w postaci `String : List<Integer>`. Metoda ma zwrócić napis.
     * W mapie przekazanej jako parametr, kluczami są litery a jako wartościami - *pozycje* na jakich mają występić w zwracanym
     * napisie.
     */

    static String zadanie126(Map<String, List<Integer>> mapa) {
        int rozmiar = 0;
        for (List<Integer> list : mapa.values()) {
            rozmiar += list.size();
        }
        String[] array = new String[rozmiar];

        for (Map.Entry<String, List<Integer>> wiersz : mapa.entrySet()) {
            for (int pozycja : wiersz.getValue()) {
                array[pozycja] = wiersz.getKey();
            }
        }
        return String.join("",array); // skleja pojedyncze literki w calego stringa [A, S, D]=> ASD
    }

    /**
     * Utwórz klasę Osoba (zawierająca pola `imie` oraz *unikalny* `identyfikator`),
     * następnie dodaj kilka osób do mapy (gdzie kluczem będzie `identyfikator` a wartością *obiekt* klasy `Osoba`) i ją wyświetl.
     */



}




