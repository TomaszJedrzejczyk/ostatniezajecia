package grupaSDA;

public class Main {

    public static void main(String[] args) {

        String wiadomosc = method("Tomasz", 66, true);
        System.out.println(wiadomosc);
        System.out.println(method("tom", 4, false));

        System.out.println(math(3));
        System.out.println(matma(22));

        metoda(67);

        System.out.println(zadanie12(5, 2));
        System.out.println(zadanie13(5, 19));

        System.out.println(zadanie14(50, 10, 40));
        System.out.println(zadanie14a(0, 0));
        System.out.println(zadanie15(0, 0, 8));
        System.out.println(zadanie15(0, 0, 0));
        System.out.println(zadanie16());
    }


    static String method(String name, int age, boolean sex) {

        String wiadomosc = "Czesc! Jestem " + name + ", mam " + age + " lata i jestem ";

        if (sex) {
            wiadomosc += " mezczyzna";
        } else {
            wiadomosc += "kobieta";
        }
        return wiadomosc;
    }

    static boolean math(int number) {
        if (number >= 20) {
            return true;
        } else {
            return false;
        }
    }

    static boolean matma(int number) {
        return number > 20;
    }

    static int metoda(int liczba) {
        if (liczba >= 20) {
            System.out.println("ta liczba jest wieksza od 20");
        } else {
            System.out.println("ta liczba jest mniejsza od 20");
        }
        return liczba;
    }

    static int zadanie12(int number, int number2) {
        if (number < number2) {
            return number;
        } else {
            return number2;
        }
    }

    static int zadanie13(int liczba, int liczba2) {
        return liczba < liczba2 ? liczba : liczba2;
    }

    static int zadanie14(int liczba, int liczba2, int liczba3) {
        if (liczba >= liczba2 && liczba >= liczba3) {
            return liczba;
        } else if (liczba2 >= liczba && liczba2 >= liczba3) {
            return liczba2;
        } else {
            return liczba3;
        }
    }

    static boolean zadanie14a(int num, int num2) {
        if (num > 0 || num2 > 0) {
            return true;
        } else {
            return false;
        }
    }

    static boolean zadanie15(int num, int num2, int num3) {
        if (num == num2 && num == num3 && num2 == num3) {
            return true;
        } else {
            return false;
        }
    }

    static int zadanie16(int num) {
        int min = 11;
        int max = 22;

        if (num < min) {
            return min - num;
        } else if (num > max) {
            return max - num;
        } else {
            return 0;
        }
    }
}


