import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JaonMain {
    public static void main(String[] args) {
//        zwracaczOsob();
        gSon();
    }

    private static List<Osoba> gSon() {
        List<Osoba> list = new ArrayList<Osoba>();
        File file = new File("people.json");
        Gson gson = new Gson();
        try {
            JsonReader jsonReader = new JsonReader(new FileReader(file));
            Osoba [] osobas = gson.fromJson(jsonReader, Osoba[].class);
            list= Arrays.asList(osobas);
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
        System.out.println(list);
        return list;
    }

    private static List<Osoba> zwracaczOsob() {
        List<Osoba> list = new ArrayList<Osoba>();
        File file = new File("people.json");
        String content = "";
        try {
            content = Files.asCharSource(file, Charsets.UTF_8).read();
        }catch (IOException e){
            e.printStackTrace();
        }
        JSONArray jsonArray = new  JSONArray(content);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject =jsonArray.getJSONObject(i);

            String imie = jsonObject.getString("first_name");
            String nazwisko =jsonObject.getString("last_name");
            int id = jsonObject.getInt("id");
            String email = jsonObject.getString("email");
            String country = jsonObject.getString("country");
            boolean vip =jsonObject.getBoolean("vip");
            System.out.println(imie);

            Osoba osoba =new Osoba(id,imie,nazwisko,email,country,vip);
            list.add(osoba);
        }
        System.out.println(list);
        return list;
    }
}
