import com.google.gson.annotations.SerializedName;
import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Osoba {
//    @SerializedName("id"); szuka w pliku podobnej nazwy
    private  int id;
    //    @SerializedName("first_name"); szuka w pliku podobnej nazwy
    private String first_name;
    //    @SerializedName("last_name"); szuka w pliku podobnej nazwy
    private String last_name;
    //    @SerializedName(email"); szuka w pliku podobnej nazwy
    private String email;
    //    @SerializedName("country"); szuka w pliku podobnej nazwy
    private  String country;
    //    @SerializedName("vip"); szuka w pliku podobnej nazwy
    private boolean vip;
}
