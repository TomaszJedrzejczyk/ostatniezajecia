import com.google.common.base.CaseFormat;
import com.google.common.base.Joiner;
import com.google.common.collect.*;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
//        method4();
//        method5();
//        method6();
        method7();

    }

    private static void method7() {
        Samochod samochod = new Samochod("Fiat", "125p", 5000, true);
        samochod.getCena();
        samochod.getMarka();
        samochod.getNazwa();

    }

    private static void method6() {
        String napis = "AlaMaKota";
        String napis2 = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, napis);
        System.out.println(napis2);
    }

    private static void method5() {
        Table<String, Integer, Boolean> map = HashBasedTable.create(); //pod jednym kluczem sa mapy, mapy w mapie
        map.put("Krychowiak", 183 , true);  //pod tymi samymi kluczami nadpisuje wartosci
        map.put("Lewandowski", 190 , false);
        map.put("Fabianski", 200 , true);
        System.out.println(map);
    }

    private static void method4() {
        BiMap<String, Integer> map = HashBiMap.create(); //w tej mapie wartosci musze byc unikalne, V: musi byc zawsze innne
        map.put("lawka", 5);
        map.put("krzeslo", 10);
        map.put("tablica", 1);
        System.out.println(map);

    }

    private static void method3() {
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("Porshe", 2000);
        map.put("Ford", 1900);
        map.put("Ferrari", 2018);
        String napis = Joiner.on(" >> ").withKeyValueSeparator(" :: ").join(map);
        System.out.println(napis);
    }

    private static void method2() {
        String[] tablica = {"Tomasz", "Grazynka", null, "Bogdan", "Michalek", null};
        String napis = Joiner.on(" | ").skipNulls().join(tablica);
        System.out.println(napis);
        String napis2 = Joiner.on(" | ").useForNull("bledna wartosc").join(tablica);
        System.out.println(napis2);
    }

    private static void method1() {
        Multimap<String, Integer> multimap = HashMultimap.create();  //moze miec powrazzajace sie klucze
        multimap.put("Tomasz", 23);
        multimap.put("Kaska", 20);
        multimap.put("Kaska", 22);
        multimap.put("Barbara", 43);
        System.out.println(multimap);

    }
}
