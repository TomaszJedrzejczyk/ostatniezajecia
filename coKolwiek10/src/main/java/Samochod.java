import lombok.*;

@AllArgsConstructor(access = AccessLevel.PUBLIC)
@EqualsAndHashCode
@ToString
@Setter
@Getter
public class Samochod {
    private String marka;
    private String nazwa;
    @Setter(AccessLevel.NONE)
    private int cena;
    private boolean czyDziala;


}
