package grupaSDA;

public class Main {

    public static void main(String[] args) {

        boolean wynik = metoda(2);
        System.out.println(wynik);
        System.out.println(metoda(43));

        System.out.println(zadanie18(10, 20, 30));
        System.out.println(zadanie18(1, 2, 3));
        System.out.println(zadanie18(1, 1, 20));
        System.out.println(calendar(3));
        System.out.println(zadanie20(2));
        System.out.println(zadanie21(2));

    }

    static boolean metoda(int liczba) {
        if ((liczba >= 10 && liczba <= 20) || (liczba >= 40 && liczba <= 50))
            return true;
        else
            return false;
    }

    static boolean zadanie18(int num, int num2, int num3) {

        int min = 10;

        if (num > num2 + min || num > num3 + min) {
            return true;
        } else if (num2 > num + min || num2 > num3 + min) {
            return true;
        } else if (num3 > num + min || num3 > num2 + min) {
            return true;
        }
        return false;
    }

    static String calendar(int month) {
        switch (month) {
            case 1:
                return "styczen";
            case 2:
                return "luty";
            case 3:
                return "marzec";
            case 4:
                return "kwiecien";
            case 5:
                return "maj";
            default:
                return "nie ma takiego miesiaca";

        }
    }

    static int zadanie20(int month) {
        int liczba = -1;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                liczba = 31;
                break;
            case 2:
                liczba = 28;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                liczba = 30;
                break;
        }
        return liczba;
    }

    static String zadanie21(int aaaaa) {
        return "ten miesiac to "+calendar(aaaaa)+" i ma "+zadanie20(aaaaa) +" dni";
    }


}
