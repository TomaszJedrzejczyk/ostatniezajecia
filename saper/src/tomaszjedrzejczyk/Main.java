package tomaszjedrzejczyk;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        boolean[][] tablica = saper(4, 4);
        wyswietlaczPlanszyBoolean(tablica);
        System.out.println();

        wyswietlaczPlanszyX(tablica);
        System.out.println();

        int[][] tablica2 = konwertowanieNaInt(tablica);
        wyswietlaczPlanszyInt(tablica2);
        System.out.println();

        tablica2 = wypelnijPlansze(tablica2);
        wypelnijPlansze(tablica2);
        wyswietlaczPlanszyInt(tablica2);
    }

    /**
     * metoda "saper" przyjmuje 2 parametry int wysokosc oraz szerokosc natomiast wynikiem jest tablica
     * wartosci logicznych true,false o wielkosci zadeklarowanej przez uzytkownika
     */

    static boolean[][] saper(int wysokosc, int szerokosc) {
        boolean[][] plansza = new boolean[wysokosc][szerokosc];
        int zageszczenie = 4;
        int liczbaBomb = wysokosc * szerokosc / zageszczenie;
        Random random = new Random();
        for (int i = 0; i < liczbaBomb; i++) {
            int x = random.nextInt(wysokosc);
            int y = random.nextInt(szerokosc);
            if (plansza[x][y]) {
                i--;
            } else
                plansza[x][y] = true;
        }
        return plansza;
    }

    /**
     * metoda "wyswietlaczPlanszyBoolean" przyjmuje jeden parametr jakim jest tablica wartosci logicznych.
     * metoda nic nie zwraca. jest to jedynie pomoc w zobrazowaniu tablic wartosci logicznych
     */

    static void wyswietlaczPlanszyBoolean(boolean[][] plansza) {
        for (int i = 0; i < plansza.length; i++) {
            for (int j = 0; j < plansza[0].length; j++) {
                System.out.print(plansza[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * metoda "wyswietlaczPlanszyX" przyjmuje jeden paramet jakim jest tablica wartosci logicznych.
     * metoda nic nie zwaraca. jej zadaniem jest podmiana wartosci true na "X" oraz wartosci false na "-"
     */

    static void wyswietlaczPlanszyX(boolean[][] plansza) {
        for (int i = 0; i < plansza.length; i++) {
            for (int j = 0; j < plansza[0].length; j++) {
                System.out.print(plansza[i][j] ? " X " : " - ");
            }
            System.out.println();
        }
    }

    /**
     *
     */

    static int[][] konwertowanieNaInt(boolean[][] plansza) {
        int[][] zwracanaPlansza = new int[plansza.length][plansza[0].length];
        for (int i = 0; i < plansza.length; i++) {
            for (int j = 0; j < plansza[0].length; j++) {
                if (plansza[i][j]) {
                    zwracanaPlansza[i][j] = -1;
                }
            }
        }
        return zwracanaPlansza;
    }

    /**
     *
     */

    static void wyswietlaczPlanszyInt(int[][] plansza) {
        for (int i = 0; i < plansza.length; i++) {
            for (int j = 0; j < plansza[0].length; j++) {
                System.out.print(plansza[i][j] + "\t");
            }
            System.out.println();
        }
    }

    /**
     *
     */

    private static int[][] wypelnijPlansze(int[][] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica[0].length; j++) {
                if (tablica[i][j] != 1) {
                    tablica[i][j] = policzBombyWOkol(tablica, i, j);
                }
            }
        }
        return tablica;
    }

    private static int policzBombyWOkol(int[][] tablica, int i, int j) {
        int wynik = 0;
        for (int k = i - 1; k <= i + 1; k++) {
            for (int l = j - 1; l <= j + 1; l++) {
                if (k >= 0 && l >= 0 && k <= tablica.length && l <= tablica[0].length) {
                    wynik++;
                }
            }
        }
        return wynik;
    }
}
